---
title: M1 Informatique Lyon 1
linkTitle: M1 informatique
date: '2024-08-30'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2024 - 2025**</span>

---


## Présentation des options du M1 printemps et des parcours M2

[Détail des UE et prérequis pour les M2](http://master-info.univ-lyon1.fr/M1/#12)

**Jeudi 7/11/2024 11h30-13h amphi 5 déambu**

|     |                                                            |                         |Indispensable   |Complémentaire  |Plus
|-----|------------------------------------------------------------|-------------------------|----------------|----------------|----
|11h30| **M2 SRS**                                                 | Thomas Begin            |                |                |
|11h45| **M2 TIW**                                                 | Nicolas Lumineau        |                |                |
|12h00| **M2 ID3D**                                                | Raphëlle Chaine         |                |                |
|12h15| **M2 IA**                                                  | Marie Lefevre           |                |                |
|12h25| M1if16 Techniques de l'IA                                  | Nadia Kabachi           | IA - DS - DISS | TIW            |
|12h30| **M2 DS**                                                  | Alexandre Aussem        |                |                |
|12h40| M1if19 Eval des perfs                                      | Alexandre Aussem        |                | DS - SRS - TIW | IA


**Vendredi 8/11/2024 9h45-11h15 amphi 5 déambu**

|     |                                                            |                         |Indispensable   |Complémentaire  |Plus
|-----|------------------------------------------------------------|-------------------------|----------------|----------------|----
| 9h45| **M2 DISS**                                                | Angela Bonifati         |                |                |
|10h00| Mif14 BD déductives                                        | Mohand-Said Hacid       | TIW - DISS     | DS - IA        |
|10h05| Mif37 Animation en synthèse d'images                       | Alexandre Meyer         | ID3D           |                |
|10h15| Mif20 Projet transversal innovant                          | Erwan Guillou           |                |                |
|10h20| Mif29 Crypto et sécurité                                   | Gérald Gavin            | SRS - TIW      |                |
|10h25| Mif26 Théorie des jeux                                     | Gérald Gavin            | IA - DS        |                |
|10h35| Mif13 Web avancé et web mobile                             | Lionel Médini           | TIW            | DS - IA        | ID3D
|10h45| Mif28 Logiciels éducatifs                                  | Stéphanie Jean-Daubias  |                | IA             | ID3D
|     |                                                            |                         |                |                |
|     | Mif27 Synthèse d'images                                    | Vincent Nivoliers       | ID3D           |                |
|     | Mif17 Analyse d'images                                     | Saida Bouakaz-Brondel   | ID3D           |                |
|     | Mif22 Parallélisme                                         | Laurent Lefevre         |                | SRS            | IA
|     | Mif32 Prog fonctionnelle avancée                           | Xavier Urbain           |                |                |

---

Toutes les informations sont sur le [site du M1 informatique](http://master-info.univ-lyon1.fr/M1/)

Réunion de rentrée : Lundi 2 septembre 2024 à 8h amphi Jussieu bâtiment Darwin [[slides]](rentree_M1_24-25.pdf)

Emplois-du-temps :
- Global [semestre d'automne](planning_M1_24-25_automne.pdf) - ATTENTION c'est indicatif, les cases blanches ne signifient pas forcément qu'il n'y a rien, seul le planning ADE ci-dessous fait foi
- Global [annuel](planning_M1_alternance_24-25.pdf) montrant les quinzaines alternants en formation et les quinzaines alternants en entreprise
- Détaillé sur [ADE](https://adelb.univ-lyon1.fr/direct/index.jsp?projectId=1&ShowPianoWeeks=true&displayConfName=DOUA_CHBIO&showTree=false&resources=9767&days=0,1,2,3,4)

---

<script>

/**
 * Plugins d'intégration dans ADE.
 */
var jours = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
var mois = ["Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet",
    "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre"];

var modal_ade;

/**
 * Formate la date et l'heure dans le popup lié à fullcalendar
 */
function dateHeureADE(event) {
    var d = new Date(event.start);
    var f = new Date(event.end);
    var s = jours[d.getDay()]
            + " " + d.getDate()
            + " " + mois[d.getMonth()]
            + " " + d.getFullYear()
            + ", " + d.getHours() + ":" + (d.getMinutes() == 0 ? "00" : d.getMinutes())
            + " - " + f.getHours() + ":" + (f.getMinutes() == 0 ? "00" : f.getMinutes());
    return s;
};

/**
 * Traite l'argument correspondant à la liste des ressources ADE à charger.
 * Réalise l'expansion des @xxx pour ADE.
 */
function expandADEResources(resources) {
    return resources.
        split('-').
        map(function(res) {
            if (res[0] == '@') {
                return ade_ids[res.substr(1)];
            } else {
                return res;
            }
        }).
        filter( function(x) { return x ; }).
        join(',');
}

/**
 * Génère une url pour un lien vers ADE.
 */
function adeLinkForDisplay(type, id, weeks, days) {
    // exemple: http://adelb.univ-lyon1.fr/direct/index.jsp?projectId=6&ShowPianoWeeks=true&displayConfName=DOUA_SCIENCES&resources=13623

    // vérifier avec les params par défaut
    if (! weeks) {
        weeks = "";
    } else {
        weeks = "&weeks=" + expandNumList(weeks);
    }
    if (! type || type === "PianoUe") {
        type = "&displayConfName=DOUA_SCIENCE";
    } else if (type === 'PianoParcours') {
        type = "&displayConfName=DOUA-INFO";
    } else {
        type = "&displayConfName="+type;
    }
    var link = "http://adelb.univ-lyon1.fr/direct/index.jsp?" + 
        "projectId=" + Constants.ade_project_id +
        "&ShowPianoWeeks=true" + 
        type +
        "&showTree=false" +
        '&resources=' + expandADEResources(id) +
        weeks +
        '&days=' + expandNumList(days);
    return link;
}

/**
 * Le plugin pour créer des liens vers ADE.
 */
var ADELINKPlugin = {
    nom: "ADELINK",
    tag: "a",
    parametres: ["texte", "type=", "weeks=", "days=0..4", "id=@ade_id"],
    render: function(elt,params,data) {
        $(elt).attr('href',adeLinkForDisplay(params.type,params.id,params.weeks,params.days));
        $(elt).html(params.texte);
    }
};
//registerExpansionPlugin(ADELINKPlugin);

/**
 * Crée une liste de jour à cacher dans fullcalendar à partir des jours à afficher.
 */
function getHiddenDays(days) {
    days = expandNumList(days);
    var daysToHide = [true, true, true, true, true, true, true];
    days.split(",").map(function(j) {
        j = parseInt(j) + 1;
        j %= 7;
        daysToHide[j] = false;
    });
    var hiddenDays = [];
    daysToHide.map(function(hide, idx) {
        if (hide) {
            hiddenDays.push(idx);
        }
    });
    return hiddenDays;
};

/**
 * Configure et ajoute le composant fullcalendar dans le bon élément.
 */
function adeFullCalendarView(elt, params) {
    //params.id = expandADEResources(params.id);
    elt.innerHTML('<div class="calloading">Chargement ...</div><div class="calview"/>');
    var calview = $('div.calview',elt);
    var calloading = $('div.calloading', elt);
    var agenda = Constants.mobile_device ? "agendaDay" : "agendaWeek";
    $(calview).fullCalendar({
        eventSources: [{
            url: Ajaxer.rewrite("/service/ade/resources/") + encodeURIComponent(params.id),
            error: function(data) {
                console.log('there was an error while fetching events!');
                console.log(data);
            },
            beforeSend: function() {
                $(calloading).show();
            },
            complete: function() {
                $(calloading).hide();
            },
            color: '#F09F1B',
            textColor: 'black',
            borderColor: '#779691',
            editable: false
        }],
        defaultView: agenda,
        timeFormat: {
            day: 'HH:mm{ - H:mm}', // 5:00 - 6:30
            week: 'HH:mm{ - H:mm}', // 5:00 - 6:30
            '': 'HH:mm', // 7:00
        },
        header: {
            left: 'title',
            center: agenda + ' month',
            right: 'today prev,next'
        },
        firstDay: 1,
        weekNumbers: true,
        /*height: 450,*/
        minTime: 7,
        maxTime: 20,
        axisFormat: 'HH:mm',
        allDaySlot: false,
        slotMinutes: 60,
        monthNames: mois,
        monthNamesShort: ['Jan', 'F&eacute;v', 'Mar', 'Avr', 'Mai', 'Juin', 
                          'Juil', "Ao&ucirc;t", "Sept", "Oct", "Nov", "D&eacute;c"],
        dayNames: jours,
        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        columnFormat: {
            month: 'ddd', // Mon
            week: 'ddd d/M', // Mon 7/9
            day: 'dddd d/M'  // Monday 7/9
        },
        hiddenDays: getHiddenDays(params.days),
        eventClick: function(event, jsEvent) {
            modal_ade.set_title(event.title);
            modal_ade.set_body("<p>" + dateHeureADE(event) + "<br/>"
                               + event.location + "</p>" + "<p>" + event.description + "</p>");
            modal_ade.open();
        },
        buttonText: {
            today: "aujourd'hui",
            month: 'mois',
            week: 'semaine',
            day: 'jour'
        },
        eventRender: function(event, element) {
            element.find(".fc-event-inner").append("<div class='fc-event-location'>" + event.location + "</div>");
        }
    });
}

/**
 * Plugin d'affichage du calendrier via fullcalendar intégré.
 */
var ADEVIEWPlugin = {
    nom: 'ADEVIEW',
    tag: 'div',
    parametres: ["days=0..4", "id=@ade_id"],
    render: function(elt,params,data) {
        if (data) {
            adeFullCalendarView(elt,params);
        }
    }
}
//registerExpansionPlugin(ADEVIEWPlugin);

//ADEVIEW(0-1-2-3-4,47718-62309-62310-19272-19275-18920-103436-103437-103567-103568)

adeFullCalendarView(window,["days=0..4", "id=@47718-62309"]);

</script>


