---
# Display name
title: Sylvain Brandel

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Associate Professor

# Organizations/Affiliations to show in About widget
organizations:
- name: Université Claude Bernard Lyon 1
  url: https://www.univ-lyon1.fr
- name : Laboratoire LIRIS
  url: https://liris.cnrs.fr

# Short bio (displayed in user profile at end of posts)
#bio: My research interests include distributed robotics, mobile computing and programmable matter.

# Interests to show in About widget
interests:
- Computer graphic
- Languages

# Education to show in About widget
education:
  courses:
  - course: PhD in Computer Science
    institution: Université de Strasbourg
    year: 2000
  - course: Master (DEA) in Computer Science
    institution: Université de Strasbourg
    year: 1995

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '/#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/Delbran0
  label: Follow me on Twitter
  display:
    header: true
#- icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
#  icon_pack: fas
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
#- icon: linkedin
#  icon_pack: fab
#  link: https://www.linkedin.com/

# Link to a PDF of your resume/CV from the About widget.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf
# {{< icon name="download" pack="fas" >}} Download my {{< staticref "uploads/resume.pdf" "newtab" >}}resumé{{< /staticref >}}.

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true

---

**Maître de conférences**\
[Université Lyon 1](http://www.univ-lyon1.fr) - [FST](http://sciences.univ-lyon1.fr/) - [Département-composante informatique](http://fst-informatique.univ-lyon1.fr/)\
[LIRIS](http://liris.cnrs.fr) - [ORIGAMI](https://liris.cnrs.fr/equipe/origami)\
Responsable du [M1 informatique Lyon 1](m1/) depuis 2018

- [Architecture des ordinateurs (L2)](archi/)
- [Logique classique (L3)](logique/)
- [Ouverture à la recherche (M1)](ouverture/)
- [Projet transversal de master informatique (M1)](projet/)
- [Modèles de calcul et complexité (M1)](calcu/)
- [Anglais pour la communication professionnelle (M1)](anglais/)

