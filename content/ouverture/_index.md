---
title: Mif11 - Ouverture à la recherche
linkTitle: Ouverture à la recherche
date: '2024-09-18'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2024 - 2025**</span>

---

## Objectif

L'objectif de cette UE est de vous confronter au monde de la recherche après une première expérience du monde de l'entreprise en fin de licence, ceci afin d'une part d’alimenter vos réflexions sur votre orientation, et d'autre part, pour ceux qui continueront vers l'entreprise, d'avoir eu une approche de la recheche.

Cette UE obligatoire (<span style="color: red">parcours classique et parcours alternant</span>) vaut 3 crédits, comme toutes les UE du M1, et correspond à un travail personnel conséquent de votre part : travail sur un projet courant de fin octobre 2024 au printemps 2025, représentant environ une demi-journée de travail par semaine.
Le projet pourra être une recherche bibliographique, une synthèse d'articles scientifiques, ou encore des développements dans le cadre d'un projet scientifique.


----

## Déroulement

* Dépôt des sujets (pour les enseignants) :
  * Date limite : vendredi 4 octobre 2024
  * Sur la page : [M1POM-enseignants](https://tomuss.univ-lyon1.fr/2024/Public/M1OuvertureRecherche)

* Présentation de l'UE
  * Mercredi 1er octobre 2024 à 11h30 amphi (précisé ultérieurement)

* Choix des sujets :
  * Date limite : vendredi 11 octobre 2024
  * Liste des sujets : [M1POM-etu](https://tomuss.univ-lyon1.fr/S/2024/Automne/public_login/2024/Public/M1OuvertureRecherche)

* Projet de recherche :
  * Dès le choix du sujet jusqu'au printemps 2025
  * Prendre RDV avec votre encadrant à l'avance à partir du 9 octobre

* Cahier des charges :
  * Date limite : **jeudi 31 octobre 2024**
  * Rédaction en interaction avec vos encadrants
  * 4 pages maximum tout compris, [exemple 1](mif11_excdc1.pdf), [exemple 2](mif11_excdc2.pdf)
  * Fichier au format .pdf intitulé cdc_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro de groupe apparaissant sur Tomuss
  * À déposer sur Tomuss, dans l'UE UE-INF1097M Projet Pour l'Orientation en Master

* Evaluation
  * Rapport à rendre fin février 2025
  * Vidéo de vulgarisation à rendre fin février 2025
  * **Pas de Soutenance**

* Rapport final :
  * Date limite : **dimanche 23 février 2025 23h59** ; aucun dépassement ne sera accordé, le projet transversal de master inforamtique (Mif10) commencera la semaine du 24 février
  * Un rapport par groupe
  * 10 pages maximum (pas de page de garde, pas de plan), annexes autorisées ne comptant pas dans les 10 pages, marges de 2cm, police Calibri (ou équivalente) 11pt, [modèle](mif11_modele.pdf) à respecter mais des adaptations sont possibles (Latex, etc.), [exemple1](mif11_exr1.pdf), [exemple2](mif11_exr2.pdf)
    * Fichier au format .pdf intitulé rapport_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro d groupe apparaissant sur Tomuss,
    * À déposer sur Tomuss, dans l'UE UE-INF1208M Ouverture à la recherche

* Video de vulgarisation de votre travail :
  * Date limite : **dimanche 23 février 2025 23h59** ; pareil que pour le rapport, date butoir non négociable
  * 3 minutes maximum
  * À déposer sur Tomuss
  * Fichier intitulé video_IDgrp_nom1_numetudiant1_nom2_numetudiant.format avec format lisible au moins avec VLC
  * La vidéo doit permettre à quelqu'un qui n'est pas du domaine de comprendre votre travail

----

## Précisions et évaluation

* Organisation pour le choix des sujets : vous consultez les sujets disponibles en ligne, puis contactez les responsables des sujets qui vous intéressent. Les sujets peuvent être traités seul ou en binôme. Les binômes peuvent être constitués par les étudiants eux-mêmes ou proposés par les enseignants.
* Remarque générale : vous apprenez à mener à bien un projet (de recherche ou de développement), vous apprenez aussi à présenter votre travail, d'où l'importance du respect des consignes.
* Conseil pour tous les écrits : attention à l'expression, l'orthographe, la grammaire, la conjugaison, la ponctuation …


