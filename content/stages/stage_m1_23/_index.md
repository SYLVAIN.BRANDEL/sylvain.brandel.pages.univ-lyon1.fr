# Stage M1 : Edition de nuage de points 3D

![](pom_point_cloud%230.png)

**Encadrants :**

- Sylvain BRANDEL - sylvain.brandel@univ-lyon1.fr

- Martin GENET - martin.genet@cnrs.fr

## Contexte et objectif

Dans le cadre du projet IASBIM (pour Intelligence Artificielle au Service du *Building Information Modeling*), en collaboration avec ses partenaires Oslandia et Bimdata, le LIRIS, via des enseignant-chercheurs et ingénieurs de l'équipe Origami, intervient entre autres pour définir, mettre en oeuvre et expérimenter des méthodes novatrices et originales de reconstruction de modèles volumiques de bâtiments à partir de **nuages de points**.

Pour tester ces méthodes, il est nécessaire de pouvoir maîtriser la **quantité** et la **qualité** des données : nombre de points dans le nuage, densité, homogénéité, bruit, ...

Il est aussi intéressant de pouvoir se référer à une **vérité-terrain** : à partir d'un nuage de point donné, quel est le résultat attendu ?

**Pour cela, on se propose de construire artificiellement des nuages de points à partir de maillages volumiques de référence, par tirage aléatoire de points sur les surfaces du modèle.**

![](pom_mesh.png)

**Image 1:** Modèle volumique de bâtiment fourni par Bimdata, rendu sur Meshlab.

![](pom_point_cloud%231.png)

**Image 2:** Nuage de point obtenu par *mesh sampling* avec la librairie PCL, rendu sur Meshlab.

![](pom_point_cloud%232.png)

**Image 3:** Intérieur du nuage de points, rendu sur Meshlab. On observe des parois, des plans parallèles et orthogonnaux qui se dégagent, ainsi que des grands volumes vides.

## Travail à réaliser

Comme illustré ci-avant, des outils existent déjà pour construire un nuage de points à partir d'un maillage, comme par exemple la méthode de *mesh sampling* offerte par la librairie PCL, mais avec un faible niveau de contrôle sur le résultat.

**Le stage proposé consisterait donc, dans un premier temps, à prendre en main les librairies PCL et CGAL (language C++) afin d'intégrer la construction de nuage de points à un pipeline de traitements complet :**

- chargement en entrée d'un modèle de maillage volumique

- pré-traitement éventuels (simplification/altération du maillage)

- construction du nuage de points (avec gestion des paramètres)

- posts-traitements : introduction de bruit, lissage, décimation, ...

**Dans un second temps, il est proposé de fournir une petite interface 3D permettant de visualiser le résultat de manière interactive, en jouant sur les différents paramètres, avec des options d'import et d'export de fichiers.**

## Notions, outils mis en oeuvre et ressources

- Géométrie algorithmique, traitement de nuage de points 3D, C++

- La *Computational Geometry Algorithms Library* (CGAL) : [CGAL 5.5 - Manual: Main Page](https://doc.cgal.org/latest/Manual/index.html)

- La *Point Cloud Library* (PCL) : https://pointclouds.org/

- M. Corsini, P. Cignoni and R. Scopigno, "Efficient and Flexible Sampling with Blue Noise Properties of Triangular Meshes," in IEEE Transactions on Visualization and Computer Graphics, vol. 18, no. 6, pp. 914-924, June 2012, doi: 10.1109/TVCG.2012.34. [Efficient and Flexible Sampling with Blue Noise Properties of Triangular Meshes](https://vcg.isti.cnr.it/Publications/2012/CCS12/TVCG-2011-07-0217.pdf)


