---
title: Mif10 - Projet transversal de master informatique
linkTitle: Projet transversal
date: '2023-12-18'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2024 - 2025**</span>

---

Le projet transversal de master informatique se déroulera du 26 février au 23 mai 2025

Page du cours : [[https://forge.univ-lyon1.fr/m1if10/2024]](https://forge.univ-lyon1.fr/m1if10/2024)

