---
title: Mif09 - Modèles de calcul et complexité
linkTitle: Modèles
date: '2025-02-19'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2024 - 2025**</span>

---

## Modèles de calcul et complexité

Page pédagogique de l'UE [Mif09 - modèles de calcul et complexité](https://perso.liris.cnrs.fr/xavier.urbain/enseignement/)

---

## Partie complexité

  * Mardi 11 mars 2025 09h45 - CM classe P et NP
  * Mardi 18 mars 2025 08h00 - TD classe P
  * Mardi 18 mars 2024 09h45 - CM NP-complétude
  * Mardi 18 mars 2025 11h00 - TD classe NP et NP-complétude


