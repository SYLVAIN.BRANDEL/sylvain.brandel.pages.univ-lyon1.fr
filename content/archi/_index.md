---
title: ARCHI - Architecture des ordinateurs
linkTitle: Architecture
date: '2024-09-05'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2024 - 2025**</span>

---

## Planning global

![Planning global](planning-ARCHI-24-25.png)


## Progression et ressources pédagogiques

| | | | | | | | |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | mardi 03/09/24 |               | | | | (Pas d'enseignements d'architecture)
|   | jeudi 05/09/24 | 14h-17h15     | [[CM0]](ARCHI-CM00.pdf) | | | Introduction
|   |                |               | [[CM1]](ARCHI-CM01.pdf) | | | Vue d'ensemble de l'ordinateur
|   |                |               | [[CM2]](ARCHI-CM02.pdf) | | | Logique propositionnelle
| 2 | mardi 10/09/24 | 8h-11h15      | | [[TD1]](ARCHI-TD1.pdf)  | | Vue d'ensemble de l'ordinateur
|   |                |               | | [[TD2]](ARCHI-TD2.pdf)  | | Logique propositionnelle
|   | jeudi 12/09/24 | 14h-17h15     | [[CM3]](ARCHI-CM03.pdf) | | | Codage des données en machine part. 1 - codage des entiers
|   |                |               | [[CM4]](ARCHI-CM04.pdf) | | | Codage des données en machine part. 2 - codage des rationnels
| 3 | mardi 17/09/24 | 8h-11h15      | | [[TD3]](ARCHI-TD3.pdf)  | | Codage des nombres en machine 
|   | jeudi 19/09/24 | 14h-15h30     | [[CM5]](ARCHI-CM05.pdf) | | | Circuits combinatoires part. 1 - logique anarchique et structurée
| 4 | mardi 24/09/24 | 8h-9h30       | | [[TD4]](ARCHI-TD4.pdf)  | | Circuits combinatoires
|   | mardi 24/09/24 | 9h45-13h      | | | [[TP1]](ARCHI-TP1.pdf)  | Circuits combinatoires part. 1 [[comp2etu.c]](tp/tp1_comp2etu.c) [[pgcdetu.circ]](tp/tp1_pgcdetu.circ)
|   | jeudi 26/09/24 | 14h-15h30     | [[CM6]](ARCHI-CM06.pdf) | | | Circuits combinatoires part. 2 - logique en tranche
| 5 | mardi 01/10/24 | 8h-11h15      | | [[TD4]](ARCHI-TD4.pdf)  | | Circuits combinatoires (suite)
|   | jeudi 03/10/24 | 14h-15h30     | [[CM7]](ARCHI-CM07.pdf) | | | Circuits séquentiels part. 1 - bascules [[démo logisim]](sequentiel.circ)
| 6 | mardi 08/10/24 | 8h-11h15      | | | [[TP2]](ARCHI-TP2.pdf)  | Circuits combinatoires part. 2 [[aluetu.circ]](tp/tp2_aluetu.circ)
|   | jeudi 10/10/24 | 14h-15h30     | [[CM8]](ARCHI-CM08.pdf) | | | Circuits séquentiels part. 2 - compteurs, automates, mémoires
| 7 | mardi 15/10/24 | 8h-11h15      | | [[TD5]](ARCHI-TD5.pdf)  | | Circuits séquentiels
|   | jeudi 17/10/24 | 14h-15h30     | [[CM9]](ARCHI-CM09.pdf) | | | Langage d'assemblage part. 1 - instructions A&L, mémoire
| 8 | mardi 22/10/24 | 8h-9h30       | | [[TD6]](ARCHI-TD6.pdf)  | | Programmation en langage d'assemblage
|   | mardi 22/10/24 | 9h45-13h      | | | [[TP3]](ARCHI-TP3.pdf)  | Circuits séquentiels
|   | jeudi 24/10/24 | 14h-15h30     | [[CMA]](ARCHI-CM10.pdf) | | | Langage d'assemblage part. 2 - routines et pile
|   |                |               |                         | | | <span style="color: red">**Vacances universitaires**</span>
| 9 | mardi 05/11/24 | 8h-9h30       | | [[TD6]](ARCHI-TD6.pdf)  | | Programmation en langage d'assemblage (suite)
|   | mardi 05/11/24 | 9h45-13h      | | | [[TP4]](ARCHI-TP4.pdf)  | Programmation en langage d'assemblage part. 1<br/> [[add_simple.asm]](tp/tp4_add_simple.asm) [[puts.asm]](tp/tp4_puts.asm) [[bin.asm]](tp/tp4_bin.asm) [[puts2.asm]](tp/tp4_puts2.asm)  [[loop.asm]](tp/tp4_loop.asm)<br/> [[lc3os.asm]](tp/lc3os.asm) 
|   | jeudi 07/11/24 |               |                         | | | (Pas d'enseignements d'architecture)
| A | mardi 12/11/24 | 8h-9h30       | | [[TD6]](ARCHI-TD6.pdf)  | | Programmation en langage d'assemblage (suite)
|   | mardi 12/11/24 | 9h45-13h      | | | [[TP5]](ARCHI-TP5.pdf)  | Programmation en langage d'assemblage part. 2<br/>[[mult6.asm]](tp/tp5_mult6.asm) [[codage.asm]](tp/tp5_codage.asm) [[chaine.asm]](tp/tp5_chaine.asm) [[saisie.asm]](tp/tp5_saisie.asm) 
|   | jeudi 14/11/24 | 14h-15h30     | [[CMB]](ARCHI-CM11.pdf) | | | Ébauche d'un processeur
| B | mardi 19/11/24 | 8h-11h15      | | | [[TP6]](ARCHI-TP6.pdf)  | Implémentation du LC-3 part. 1 [[LC3_etu.circ]](tp/LC3_etu.circ) [[add_simple.mem]](tp/tp6_add_simple.mem)
|   | jeudi 21/11/24 | 14h-15h30     | [[CMC]](ARCHI-CM12.pdf) | | | Mémoire cache et pipeline
| C | mardi 26/11/24 | 8h-11h15      | | | [[TP7]](ARCHI-TP7.pdf)  | Implémentation du LC-3 part. 2
|   | jeudi 28/11/24 |               |                         | | | (Pas d'enseignements d'architecture)
| D | mardi 03/12/24 | 8h-9h30       | ÉCA                     | | | **Épreuve Commune Anonyme**|
|   | mardi 03/12/24 | 9h45-13h      | | | TPN                     | **TP noté**|
|   | jeudi 05/12/24 |               |                         | | | (Pas d'enseignements d'architecture)
<!--
-->

### Ressources complémentaires

  * TP [[Mémo de TP]](ARCHI-TP-memo.pdf), [[Logisim]](logisim-generic-2.7.1.jar), [[Pensim]](PennSim.jar)

  * [Exercices interactifs](https://perso.liris.cnrs.fr/vincent.nivoliers/suzette.php?exo=bintable&query=create&ticket=demain&html=true) proposés par Vincent Nivoliers

----

## Organisation

### Responsable

  * ARCHI - Architecture des ordinateurs : Sylvain Brandel

### Volume

  * CM : 18 heures (12 x 1h30)
  * TD : 18 heures (4 x 3h et 4 x 1h30)
  * TP : 24 heures (8 x 3h)

### Horaires

  * Tous les enseignements de ARCHI ont lieu dans la séquence 3 (mardi matin et jeudi après-midi)
  * Globalement, les CM ont lieu le jeudi après-midi de 14h à 15h30, les TD / TP le mardi matin de 8h à 11h15 ou 13h
  * Le jeudi après-midi il n'y aura jamais autre chose que du CM (pas d'évaluations)

### Intervenants et salles

|                     | CM - amphi | TD - salle     | TP - salle   |
| ---                 | ---        | ---            | ---          |
|Sylvain Brandel      | Déambu 4   |A - Thémis 68   |A1 - Ariane 01|
|                     |            |                |A2 - Ariane 02|
|Vincent Nivoliers    |            |B - Oméga 11    |B1 - Ariane 03|
|                     |            |                |B2 - Ariane 04|
|Xavier Urbain        |            |C - Grignard 25 |C1 - Ariane 05|
|                     |            |                |C2 - Ariane 06|
|Gaspard Thévenet     |            |D - Thémis 61   |D1 - Ariane 11|
|Elise Jeanneau       |            |                |D2 - Ariane 12|
|Gabriel Meynet       |            |E - Thémis 53   |E1 - Ariane 13|
|Franck Favetta       |            |                |E2 - Ariane 14|

### Evaluation

  * **Contrôle continu intégral** :
    * ÉCA mardi 3 décembre 2024 à 8h
    * TP noté mardi 3 décembre 2024 à partir de 9h45
    * Interros en début de TD, peut-être un autre TP noté ...
  * **Règles d'absences aux contrôles de TD** :
    * Zéro en cas d'absence à un contrôle de TD, justifiée ou non, quelle qu'en soit la raison
    * Pas de rattrapage des contrôles en cas d'absence (légère tolérence au retard)
    * La moyenne des n contrôles sera calculée à partir des (n-1) meilleures notes
    * Le jury d'UE discutera des cas particuliers (absence longue durée, absences répétées ...)

---

## Annales

  * [ÉCA 2019](annales/LifASR4-ECA-2019.pdf)
  * [ÉCA 2020](annales/LifASR4-ECA-2020.pdf)

  * [TP noté 2020](annales/LifASR4-TPNOTE-2020.pdf)

