---
title: Mif09 - Modèles de calcul et complexité
linkTitle: old/calcu22
date: '2023-03-01'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## Modèles de calcul et complexité

Page pédagogique de l'UE [Mif09 - modèles de calcul et complexité](https://perso.liris.cnrs.fr/xavier.urbain/enseignement/)

---

## Partie complexité

  * Mardi 14 mars 2023 - CM [classe P et NP](M1if09-CM07.pdf)
  * Mardi 21 mars 2023 - CM [NP-complet](M1if09-CM08.pdf)


