---
title: Mif10 - Projet transversal de master informatique
linkTitle: old/projet22
date: '2023-03-01'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## Planning des soutenances du mardi 20 juin 2023

<span style="color: red">PLANNING DEFINITIF</span> (à quelques ajustements mineurs potentiels près)

### Jury 1 - Quai 43 salle 111

|       | Président | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 | Etudiant 3 |
| ----- | --------- | ------ | ----- | ---------- | ---------- | ---------- |
| 13h   | H. KHEDDOUCI | N. GUIN | 1 | Roméo PHANG |  | 
| 13h30 | H. KHEDDOUCI | N. GUIN | 2 | Augustin LAOUAR | Thinhinane ZEDDAM | 
| 14h   | H. KHEDDOUCI | N. GUIN | 4 | Stanislas BAGNOL | Mathis SPATARO | 
| 14h30 | H. KHEDDOUCI | N. GUIN | 5 | Baptiste GENEST |  | 
| 15h   | H. KHEDDOUCI | N. GUIN | 8 | Jessie JEAN-LOUIS | Selma LOUAHEM M'SABAH | 
| 15h30 | H. KHEDDOUCI | N. GUIN | 9 | Alexis BONIS | Ophélie BROSSE | 
| 16h   | H. KHEDDOUCI | F. DUCHATEAU | 10 | Elodie BOURBON |  | 
| 16h30 | H. KHEDDOUCI | F. DUCHATEAU | 12 | Ahmed CISSE | Khaled MAHAZZEM | 
| 17h   | H. KHEDDOUCI | F. DUCHATEAU | 14 | Lucas PETIT | Thomas STAVIS | 
| 17h30 | H. KHEDDOUCI | F. DUCHATEAU | 15 | Victor FAURE | Nino RAVELLA | 

### Jury 2 - Quai 43 salle 112

|       | Président | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 | Etudiant 3 |
| ----- | --------- | ------ | ----- | ---------- | ---------- | ---------- |
| 13h   | I. GUERIN-LASSOUS | Th. BEGIN | 36 | Antoine BILLOD | Loan BURGER | 
| 13h30 | I. GUERIN-LASSOUS | Th. BEGIN | 39 | Samuel YANG |  | 
| 14h   | I. GUERIN-LASSOUS | Th. BEGIN | 40 | Loïc CHASSIN  |  | 
| 14h30 | I. GUERIN-LASSOUS | Th. BEGIN | 42 | Kevin TANG | David TRAN | Anh-Kiet VO
| 15h   | I. GUERIN-LASSOUS | K. BENABDESLEM | 43 | Anna-Ludvina CAYUELA | Sonia DAKHLI | 
| 15h30 | I. GUERIN-LASSOUS | K. BENABDESLEM | 37 | Juba TERRAS |  | 
| 16h   | I. GUERIN-LASSOUS | K. BENABDESLEM | 47 | Nawresse ACHOUR |  | 
| 16h30 | I. GUERIN-LASSOUS | K. BENABDESLEM | 48 | Siham KIARED |  | 
| 17h   | I. GUERIN-LASSOUS | K. BENABDESLEM | 49 | Mohamed Islam DRICI | Adam VISINGIRIEV | 
| 17h30 | I. GUERIN-LASSOUS | K. BENABDESLEM | 50 | Iliesse LARIBI | Yann VINCENT | 

### Jury 3 - Quai 43 salle 113

|       | Président | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 | Etudiant 3 |
| ----- | --------- | ------ | ----- | ---------- | ---------- | ---------- |
| 13h   | S. AKNINE | M. MOY | 16 | Dylan JEANNIN | Bastien RUIVO | 
| 13h30 | S. AKNINE | M. MOY | 21 | Francis BEVALOT | Christophe MARTINEZ | 
| 14h   | S. AKNINE | M. MOY | 22 | Ildes RAMPON |  | 
| 14h30 | S. AKNINE | M. MOY | 23 | Gabriel JUSTIN |  | 
| 15h   | S. AKNINE | M. MOY | 32 | Tom CLABAULT | Constantin MAGNIN | Axel MESEGUER
| 15h30 | S. AKNINE | M. MOY | 33 | Léa ROUILLER |  | 
| 16h   | S. AKNINE | D. COEURJOLLY | 38 | Félix BARDY |  |
| 16h30 | S. AKNINE | D. COEURJOLLY | 35 | Imane BAHADI |  | 
| 17h   | S. AKNINE | E. COQUERY | 44 | Hind ATBIR | Axel BESSY | 
| 17h30 | S. AKNINE | E. COQUERY | 34 | Thibault TREMBLEAU |  | 

### Jury 4 - Quai 43 salle 114

|       | Président | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 | Etudiant 3 |
| ----- | --------- | ------ | ----- | ---------- | ---------- | ---------- |
| 13h   | S. JEAN-DAUBIAS | M. LEFEVRE | 11 | Bachekou DIABY | Charles ENG | Nicolas LERAY
| 13h30 | S. JEAN-DAUBIAS | M. LEFEVRE | 17 | Florian GEILLON | Preslav TENEV | 
| 14h   | S. JEAN-DAUBIAS | A. MEYER | 18 | Julien LEFEBVRE | Loris MERCIER | 
| 14h30 | S. JEAN-DAUBIAS | A. MEYER | 19 | Adem FERADJI | Hicham RKIBA | 
| 15h   | S. JEAN-DAUBIAS | G. DAMIAND | 20 | Nicolas GACHOT |  | 
| 15h30 | S. JEAN-DAUBIAS | G. DAMIAND | 24 | Ilias NAOUZ | Imed Eddine Amine SAIDOUNI | 
| 16h   | S. JEAN-DAUBIAS | G. DAMIAND | 25 | Yasmine BOUAMRA |  | 
| 16h30 | S. JEAN-DAUBIAS | G. DAMIAND | 26 | Chloé CONRAD |  | 
| 17h   | S. JEAN-DAUBIAS | G. DAMIAND | 29 | Christophe PLAIGE |  | 
| 17h30 | S. JEAN-DAUBIAS | G. DAMIAND | 30 | Hawa FOFANA | Grace TSOUALLA TATIDOUNG | 



---

## Objectif

Cette année, le projet transveral de master inforamtique sera la continuité du travail effectué dans le cadre de l'UE [[Ouverture à la recherche]](ouverture).
* Le travail de développement sera évalué dans le cadre de l'UE [[Ouverture à la recherche]](ouverture),
* La partie gestion de projet (cahier des charges, rapport et soutenance) sera évaluée dans le cadre de l'UE Projet transversal.

Pour la seconde partie, seuls sont concernés les étudiants en formation classique (non alternants).


----

## Déroulement

* Rapport final :
  * Date limite : **2 juin 2023**
  * Un rapport par binôme
  * 10 pages maximum (pas de page de garde, pas de plan), annexes autorisées ne comptant pas dans les 10 pages, marges de 2cm, police Calibri (ou équivalente) 11pt, [modèle](mif11_modele.pdf) à respecter mais des adaptations sont possibles (Latex, etc.), [exemple1](mif11_exr1.pdf), [exemple2](mif11_exr2.pdf)
    * Fichier au format .pdf intitulé rapport_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro d groupe apparaissant sur Tomuss,
    * À déposer sur Tomuss, dans l'UE UE-INF1096M Projet transversal de master informatique
    * Plutôt que déposer un pdf, vous pouvez indiquer une URL où trouver votre rapport

* Soutenances :
  * **Mardi 20 juin 2023 entre 13h et 18h**, le planning détaillé sera précisé ultérieurement sur cette même page
  * Une soutenance par binôme
  * 15 minutes de présentation + 5 minutes de questions. <span style="color: red">Respectez bien le timing</span>, la note de soutenance en tiendra compte
  * La soutenance devra mettre en évidence la problématique de votre projet et votre contribution
  * Si vous êtes à deux (ou trois), veillez à vous répartir équitablement lors de la présentation
  * Vous pourrez faire une démo, dans le temps imparti de la présentation

* Barème indicatif :
  * Note du rapport évalué par deux rapporteurs : coef. 2 (1 par rapporteur)
  * Note de la soutenance évaluée par le jury de soutenance : coef. 2

----

## Précisions et évaluation

* Remarque générale : vous apprenez à mener à bien un projet (de recherche ou de développement), vous apprenez aussi à présenter votre travail, d'où l'importance du respect des consignes.
* Conseil pour tous les écrits : attention à l'expression, l'orthographe, la grammaire, la conjugaison, la ponctuation …


