---
title: Mif09 - Modèles de calcul et complexité
linkTitle: old/calcu23
date: '2023-12-18'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2023 - 2024**</span>

---

## Modèles de calcul et complexité

Page pédagogique de l'UE [Mif09 - modèles de calcul et complexité](https://perso.liris.cnrs.fr/xavier.urbain/enseignement/)

---

## Partie complexité

  * Mardi 12 mars 2024 8h00 - CM classe P et NP
  * Mardi 12 mars 2024 9h45 - CM NP-complétude



