---
title: LifASR4 - Architecture des ordinateurs
linkTitle: old/archi21
date: '2021-11-30'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2021 - 2022**</span>

---

## Progression

| | | | |
| --- | --- | --- | --- |
| S1 | jeudi 09/09/21 14h-15h30| **CM** | [CM0](LifASR4-CM00.pdf) Introduction<br/> [CM1](LifASR4-CM01.pdf) Vue d'ensemble de l'ordinateur |
|    | jeudi 09/09/21 15h45-17h15| **CM** | [CM2](LifASR4-CM02.pdf) Codage des données en machine partie 1 - codage des entiers |
| S2 |mardi 14/09/21 8h-11h15 |TD|Chapitre 1 - Vue d'ensemble de l'ordinateur, Chapitre 2 - Codage des nombres
|    |jeudi 16/09/21 14h-15h30|**CM** |[CM3](LifASR4-CM03.pdf) Codage des données en machine partie 2 - codage des rationnels|
|    |jeudi 16/09/21 15h45-17h15|**CM** |[CM4](LifASR4-CM04.pdf) Logique propositionnelle|
| S3 |mardi 21/09/21 8h-9h30|TD|Chapitre 3 - Logique propositionnelle|
|    |mardi 21/09/21 9h45-11h15|TP|TP1 - Introduction à LC-3 ([fichiers](lifasr4_tp1.zip))|
|    |jeudi 23/09/21 14h-15h30|**CM** |[CM5](LifASR4-CM05.pdf) Circuits combinatoires partie 1 - logique anarchique et structurée|
| S4 |mardi 28/09/21 8h-11h15|TD|Chapitre 2 - Codage des nombres (suite)<br/> Chapitre 4 - Circuits combinatoires|
|    |jeudi 30/09/21 14h-15h30|**CM** |[CM6](LifASR4-CM06.pdf) Circuits combinatoires partie 2 - logique en tranche|
| S5 |mardi 05/10/21 8h-11h15|TP|TP2 prise en main de LogiSim|
|    |jeudi 07/10/21 14h-15h30|**CM** |[CM7](LifASR4-CM07.pdf) Circuits séquentiels partie 1 - bascules|
| S6 |mardi 12/10/21 8h-9h30|TD|Chapitre 4 - Circuits combinatoires (suite)|
|    |mardi 12/10/21 9h45-11h15|TP|TP3 - circuits combinatoires ([fichiers](lifasr4_tp3.zip))|
|    |jeudi 14/10/21 14h-15h30|**CM** |[CM8](LifASR4-CM08.pdf) Circuits séquentiels partie 2 - compteurs, registres, automates et RAM<br/> ([démo LogiSim](sequentiel.circ.zip))|
| S7 |mardi 19/10/21 8h-11h15|TP|TP4 circuits séquentiels|
|    |jeudi 21/10/21 14h-15h30|**CM** |[CM9](LifASR4-CM09.pdf) Langage d'assemblage du LC-3 partie 1 - instructions A&L, accès mémoire|
|    | | | Vacances universitaires |
| S8 |mardi 02/11/21 8h-9h30|TD|Chapitre 5 - Circuit séquentiels|
|    |mardi 02/11/21 9h45-11h15|TP|TP5 - circuits dédiés ([fichiers](lifasr4_tp5.zip))|
|    |jeudi 04/11/21 14h-15h30|**CM** |[CM10](LifASR4-CM10.pdf) Langage d'assemblage du LC-3 partie 2 - routines et pile|
|    |jeudi 04/11/21 15h45-17h15|**CM** |[CM11](LifASR4-CM11.pdf) Ébauche d'un processeur|
| S9 |mardi 09/11/21 8h-11h15|TP|TP6 - programmation LC-3 ([fichiers](lifasr4_tp6.zip))|
|    |mardi 09/11/21 **11h30-13h**|TP|<span style="color: red">groupe A</span> TP6 - programmation LC-3 (suite)|
|    |jeudi 11/11/20| | (jeudi férié) |
| S10|mardi 16/11/21 8h-9h45|TD|Chapitre 6 - Programmation en langage d'assemblage|
|    |mardi 16/11/21 9h45-11h15|TP|<span style="color: red">groupes BCDE</span> TP6 programmation LC-3 (suite)|
|    |mardi 16/11/21 9h45-**13h**|TP|<span style="color: red">groupe A</span> TP7 - implémentation du LC-3 partie 1 ([fichiers](lifasr4_tp7.zip))|
|    |jeudi 18/11/21 14h-15h30 |**CM** |[CM12](LifASR4-CM12.pdf) - Mémoire cache et pipeline|
| S11|mardi 23/11/21 8h-11h15|TD|Chapitre 6 - Programmation en langage d'assemblage (suite)<br/> Chapitre 7 - Mémoire cache et pipeline|
|    |mardi 23/12/21 **11h30-13h**|TP|<span style="color: red">groupe A</span> TP8 implémentation du LC-3 partie 2|
|    |**jeudi 25/11/21 14h-17h15**|TP|<span style="color: red">groupes BCDE</span> TP7 - implémentation du LC-3 partie 1 ([fichiers](lifasr4_tp7.zip))|
| S12|mardi 30/11/21 8h-9h30|**ÉCA**|Épreuve Commune Anonyme|
|    |mardi 30/12/21 **11h30-13h**|TP|<span style="color: red">groupe A</span> TP8 implémentation du LC-3 partie 2 (suite)|
|    |**jeudi 02/12/21 14h-17h15**|TP|<span style="color: red">groupes BCDE</span> TP8 implémentation du LC-3 partie 2|
| S13|mardi 07/12/21 8h-11h15|**TPN**|TP noté|
|    |mardi 07/12/21 **11h30-13h**|TD|<span style="color: red">groupe A</span> Chapitre 7 - mémoire cache et pipeline (suite) - Correction de l'ÉCA|
|    |**jeudi 09/12/21 14h-15h30**|TD|<span style="color: red">groupes BCDE</span> Chapitre 7 - mémoire cache et pipeline (suite) - Correction de l'ÉCA|

  * TD [Cahier de TD](lifasr4_cahier_td_automne2021.pdf)
  * TP [Cahier de TP](lifasr4_cahier_tp_automne2021.pdf)

  * [Exercices interactifs](https://perso.liris.cnrs.fr/vincent.nivoliers/suzette.php?exo=bintable&query=create&ticket=demain&html=true) proposés par Vincent Nivoliers

----

## Organisation

### Responsable

  * LifASR4 - Architecture des ordinateurs : Sylvain Brandel

### Volume

  * CM : 20 heures (13 x 1h30)
  * TD : 16 heures (11 x 1h30)
  * TP : 24 heures (16 x 1h30)

### Horaires

  * Tous les enseignements de LifASR4 ont lieu dans la séquence 3 (mardi matin et jeudi après-midi)
  * Globalement, les CM ont lieu le jeudi après-midi de 14h à 15h30, les TD / TP le mardi matin de 8h à 11h15

### Intervenants

| | | | |
| --- | --- | --- | --- |
|Sylvain Brandel|CM|TD gr. A|TP gr. A1|
|François Pitois| | |TP gr. A2|
|Vincent Nivoliers| |TD gr. B|TP gr. B1|
|Qi Qiu| | |TP gr. B2|
|Xavier Urbain| |TD gr. C|TP gr. C1|
|Calvin Abou-Haidar| | |TP gr. C2|
|Enguerrand Prebet| |TD gr. D|TP gr. D1|
|Franck Favetta| | |TP gr. D2|
|Thaïs Baudon| |TD gr. E|TP gr. E1|
|Yacine Gaci| | |TP gr. E2|

### Evaluation

  * **Contrôle continu intégral**  :
      * ECA mardi 30 novmbre 2021 8h
      * TP noté mardi 7 décembre 2021 8h
      * Interros en début de TD, peut-être un autre TP noté …

---

## Annales

  * [ÉCA 2019](LifASR4-ECA-2019.pdf)
  * [ÉCA 2020](LifASR4-ECA-2020.pdf)

  * [TP noté 2020](LifASR4-TPNOTE-2020.pdf)

