---
title: Mif11 - Ouverture à la recherche
linkTitle: old/ouverture23
date: '2022-09-09'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2023 - 2024**</span>

---

## Objectif

L'objectif de cette UE est de confronter les étudiants au monde de la recherche après une première expérience du monde de l'entreprise en fin de licence, ceci afin d'une part d’alimenter les réflexions des étudiants sur leur orientation, et d'autre part, pour ceux qui continueront vers l'entreprise, d'avoir eu une approche de la recheche.

Cette UE obligatoire vaut 3 crédits et correspond à un travail personnel conséquent de la part de l'étudiant : un projet courant de fin octobre 2023 au printemps 2024, représentant environ une demi-journée de travail par semaine.
Le projet pourra être une recherche bibliographique, une synthèse d'articles scientifiques, ou encore des développements dans le cadre d'un projet scientifique.


----

## Déroulement

* Dépôt des sujets (pour les enseignants) :
  * Date limite : 9 octobre 2023
  * Sur la page : [M1POM-enseignants](https://tomuss.univ-lyon1.fr/2023/Public/M1OuvertureRecherche)

* Présentation de l'UE
  * Mercredi 11 octobre 2023 à 11h30 amphi DEPERET

* Choix des sujets :
  * Date limite : 18 octobre 2023
  * Liste des sujets : [M1POM-etu](https://tomuss.univ-lyon1.fr/S/2023/Automne/public_login/2023/Public/M1OuvertureRecherche)

* Projet de recherche :
  * Dès le choix du sujet jusqu'au printemps 2024
  * Prendre RDV avec votre encadrant à l'avance à partir du 16 octobre

* Cahier des charges :
  * Date limite : **31 octobre 2023**
  * Rédaction en interaction avec vos encadrants à partir de novembre
  * 4 pages maximum tout compris, [exemple 1](mif11_excdc1.pdf), [exemple 2](mif11_excdc2.pdf)
  * Fichier au format .pdf intitulé cdc_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro de groupe apparaissant sur Tomuss
  * À déposer sur Tomuss, dans l'UE UE-INF1097M Projet Pour l'Orientation en Master

* Evaluation
  * Rapport à rendre fin février 2024
  * Vidéo de vulgarisation à rendre fin février 2024
  * **Pas de Soutenance**

* Rapport final :
  * Date limite : **dimanche 25 février 2024 23h59**
  * Un rapport par groupe
  * 10 pages maximum (pas de page de garde, pas de plan), annexes autorisées ne comptant pas dans les 10 pages, marges de 2cm, police Calibri (ou équivalente) 11pt, [modèle](mif11_modele.pdf) à respecter mais des adaptations sont possibles (Latex, etc.), [exemple1](mif11_exr1.pdf), [exemple2](mif11_exr2.pdf)
    * Fichier au format .pdf intitulé rapport_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro d groupe apparaissant sur Tomuss,
    * À déposer sur Tomuss, dans l'UE UE-INF1208M Ouverture à la recherche

* Video de vulgarisation de votre travail :
  * Date limite : **dimanche 25 février 2024 23h59**
  * 3 minutes maximum
  * À déposer sur Tomuss
  * Fichier intitulé video_IDgrp_nom1_numetudiant1_nom2_numetudiant.format avec format lisible au moins avec VLC
  * La vidéo doit permettre à quelqu'un qui n'est pas du domaine de comprendre votre travail

<!--
* Rendu final :
  * Date limite : **31 mars / 21 avril 2023** (cf. ci-dessous)
  * Un rendu par binôme, sous forme de **dépot de code** (forge de l'université, forge du LIRIS, autre GitLAB, GitHUB ...)
  * Dans le dépot de code, le descriptif (.md) présentera
    * Le travail effectué et la documentation pour le travail de développement effectué (s'il y a eu du travail de développement)
    * Le résumé d'articles et le rapport d'avancement si le travail effectué était sous forme d'une étude bibliographique
  * URL à indiquer sur Tomuss, dans l'UE UE-INF1208M Ouverture à la recherche
  * Date limite :
    * URL à indiquer dans TOMUSS au plus tard le **31 mars 2023**
    * Modification du code / du rapport d'avancement jusqu'au **21 avril 2023**

* Rapport et Soutenance :
  * <span style="color: red"> Seront organisés dans le cadre de l'UE-INF1096M Projet transversal de master informatique</span>
  * Ne concerne donc que ceux qui y sont inscrits (formation classique uniquement)

* Video de vulgarisation de votre travail :
  * Date limite : **21 avril 2023**
  * 3 minutes maximum
  * À déposer sur Tomuss
  * Fichier intitulé video_IDgrp_nom1_numetudiant1_nom2_numetudiant.format avec format lisible au moins avec VLC
  * La vidéo doit permettre à quelqu'un qui n'est pas du domaine de comprendre votre travail

* Note du travail (pour les enseignants) : Mardi 20 juin 2023

* Barème indicatif :
  * Note du travail évalué par l'encadrant : coef. 3
  * Note du code déposé : coef. 2
  * Note de la vidéo de vulgarisation : coef. 1
-->

----

## Précisions et évaluation

* Organisation pour le choix des sujets : vous consultez les sujets disponibles en ligne, puis contactez les responsables des sujets qui vous intéressent. Les sujets peuvent être traités seul ou en binôme. Les binômes peuvent être constitués par les étudiants eux-mêmes ou proposés par les enseignants.
* Remarque générale : vous apprenez à mener à bien un projet (de recherche ou de développement), vous apprenez aussi à présenter votre travail, d'où l'importance du respect des consignes.
* Conseil pour tous les écrits : attention à l'expression, l'orthographe, la grammaire, la conjugaison, la ponctuation …


