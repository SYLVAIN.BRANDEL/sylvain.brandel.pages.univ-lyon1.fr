---
title: LifLF - Langages formels - HCMUS
linkTitle: old/HCMUS21
date: '2023-04-06'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**AVRIL 2023**</span>

---

## Ressources pédagogiques

  * Supports de cours : 
    * [[CM0 - Introduction]](LF-CM00.pdf)
    * [[CM1 - Rappels maths]](../../langages/LF-CM01.pdf)
    * [[CM2 - Alphabets et langages]](../../langages/LF-CM02.pdf)
    * [[CM3 - Grammaires]](../../langages/LF-CM03.pdf)
    * [[CM4 - Automates à états finis]](../../langages/LF-CM04.pdf)
    * [[CM5 - Déterminisation]](../../langages/LF-CM05.pdf)
    * [[CM6 - Caractérisation]](../../langages/LF-CM06.pdf)
    * [[CM7 - Minimisation des états]](../../langages/LF-CM07.pdf)
    * [[CM8 - Langages rationnels - rationalité]](../../langages/LF-CM08.pdf)
    * [[CM9 - Automates à pile - algébricité]](../../langages/LF-CM09.pdf)
    * [[CM10 - Analyse ascendante]](../../langages/LF-CM10.pdf)

  * [[Exercices de TD]](LF-TD.pdf)


----

