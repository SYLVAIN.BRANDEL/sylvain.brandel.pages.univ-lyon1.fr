---
title: M1 Informatique Lyon 1
linkTitle: old/M121
date: '2021-11-19'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2021 - 2022**</span>

---

Toutes les informations sont sur le [site du M1 informatique](http://master-info.univ-lyon1.fr/M1/)

---

## Présentation du master informatique

Jeudi 14 avril 2022 16h amphi Thémis 10 - [slides](https://perso.liris.cnrs.fr/sylvain.brandel/doc/M1/presentation_L3_vers_M1_21-22.pdf) - [vidéo](https://perso.liris.cnrs.fr/sylvain.brandel/doc/M1/presentation_L3_vers_M1_21-22.mp4)

---

## Présentation des parcours M2

**mardi 23/11/2021 13h-14h amphi Astrée 13**

|               |     |
|-------------- |-----|-----------
|Mardi 23/11/21 |13h  | [M2 SRS](data/Pres_M2SRS_2021.pdf) - Thomas Begin
|               |13h10| [M2 ID3D](data/Pres_M2ID3D_2021.pdf) - Raphaëlle Chaine
|               |13h20| [M2 DISS](data/Pres_M2DISS_2021.pdf) - Angela Bonifati
|               |13h30| [M2 TIW](data/Pres_M2TIW_2021.pdf) - Emmanuel Coquery / Fabien De Marchi
|               |13h40| 
|               |13h50|
|Jeudi 25/11/21 |13h  | [M2 DS](data/Pres_M2DS_2021.pdf) - Alexandre Aussem 

Pour le [M2 IA](http://master-info.univ-lyon1.fr/IA/) : [salima.hassas@univ-lyon1.fr](mailto:salima.hassas@univ-lyon1.fr)

---

## Présentation des options du M1-S2

[Détail des UE et prérequis pour les M2](http://master-info.univ-lyon1.fr/M1/#nom16) (page réparée)

**Jeudi 25/11/2021 13h-14h amphi ~~Thémis 9~~ Thémis 11**

|     |                                                                 |                         |Indispensable|Complémentaire |Plus
|-----|-----------------------------------------------------------------|-------------------------|-------------|---------------|----
|13h  | [M2 DS](data/Pres_M2DS_2021.pdf)                                | Alexandre Aussem        |             |               |
|13h10| [M1if19 éval des perfs](data/Pres_M1if19_2021.pdf)              | Alexandre Aussem        |             | DS - SRS - TIW| IA
|13h15| [M1if25 trait. signal et comm. num.](data/Pres_M1if25_2021.pdf) | Florent Dupont          | SRS         | ID3D          |
|13h20| [M1if22 parallélisme](data/Pres_M1if22_2021.pdf)                | Laurent Lefèvre         | SRS         |               | IA
|13h25| [M1if13 web avancé et mobile](data/Pres_M1if13_2021.pdf)        | Lionel Médini           | TIW         | DS - IA - SRS | ID3D
|13h30| [M1if18 systèmes avancés](data/Pres_M1if18_2021.pdf)            | Grégoire Pichon         |             | SRS           |
|13h35| [M1if15 réseaux par la pratique](data/Pres_M1if15_2021.pdf)     | Isabelle Guerin-Lassous | SRS         |               |


**Vendredi 26/11/2021 13h-14h amphi Thémis 11**

|     |                                                                 |                         |Indispensable   |Complémentaire |Plus
|-----|-----------------------------------------------------------------|-------------------------|----------------|---------------|----
|13h  | [M1if29 crypto et sécurité](data/Pres_M1if29_2021.pdf)          | Gérald Gavin            | SRS - TIW      |               |
|13h05| [M1if26 théorie des jeux](data/Pres_M1if26_2021.pdf)            | Gérald Gavin            | IA - DS        |               |
|13h10| [M1if27 synthèse d'image](data/Pres_M1if27_2021.pdf)            | Vincent Nivoliers       | ID3D           |               |
|13h15| M1if37 animation                                                | Alexandre Meyer         | ID3D           |               |
|13h20| [M1if12 algorithmique distribuée](data/Pres_M1if12_2021.pdf)    | Elise Jeannot           | SRS - TIW      | ID3D - IA     |
|13h25| [M1if28 logiciels éducatifs](data/Pres_M1if28_2021.pdf)         | Stéphanie Jean-Daubias  | IA             |               | ID3D
|13h30| [M1if17 analyse d'image](data/Pres_M1if17_2021.pdf)             | Saida Bouakaz-Brondel   | ID3D - DISS    |               | IA
|13h35| [M1if14 BD déductives](data/Pres_M1if14_2021.pdf)               | Angela Bonifati         | TIW - DISS     | DS - IA       |
|13h40| [M1if20 projet transversal innovant](data/Pres_M1if20_2021.pdf) | Erwan Guillou           |                | ID3D          |
|13h45| [M1if16 techniques d'IA](data/Pres_M1if16_2019.pdf)             | Nadia Kabachi           | DS - IA - DISS | TIW           |

[Détail des UE et prérequis pour les M2](http://master-info.univ-lyon1.fr/M1/#nom16) (page réparée)

![Semestre 2](data/planning_M1_21-22_S2.png)

