---
title: M1 Informatique Lyon 1
linkTitle: old/M122
date: '2022-08-31'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## Planning des épreuves de rattrapage

<span style="color: red">**Version DEFINITIVE**</span>

Toutes les épreuves se passeront dans l'amphi AMPERE (bâtiment Lipmann),
sauf Mif04 Apprentissage vendredi 29 juin à 9h45 au Quai 43 salles 111 à 114.

|                  |     |           |
|----------------- |-----|-----------|--------------
|Lundi 26/06/23    |08h  | Mif15 Réseau par la pratique
|                  |09h45| **Mif12** Algo dist
|                  |11h30| Mif17 Analyse d'images
|                  |14h  | **Mif07** Algorithmique pour l'optmisation
|                  |15h45| Mif23 Prog avancée
|                  |17h30|
|Mardi 27/06/23    |08h  |
|                  |09h45| Mif24 BDNoSQL | Mif27 Synthèse d'images
|                  |11h30| **Mif01** GL
|                  |14h  | **Mif08** Compilation
|                  |15h45| Mif29 Crypto et sécurité
|                  |17h30|
|Mercredi 28/06/23 |08h  | **Mif09** Modèles de calcul
|                  |09h45| Mif19 Eval des perfs
|                  |11h30| Mif37 Animation en synthèse d'images
|                  |14h  | **Mif03** Programmation web
|                  |15h45| Mif13 Web avancé
|                  |17h30|
|Jeudi 29/06/23    |08h  | **Mif05** Réseau
|                  |09h45| Mif25 Traitement du signal
|                  |11h30|
|                  |14h  | Mif14 BD déductives
|                  |15h45| Mif22 Parallélisme
|                  |17h30|
|Vendredi 30/06/23 |08h  | Mif18 Systèmes avancés
|                  |09h45| **Mif04** Apprentissage <span style="color: red">Quai 43 s. 111, 112, 113, 114</span>
|                  |11h30|
|                  |14h  | Mif02 Image
|                  |15h45| Mif06 BIA
|                  |17h30|
|Lundi 03/07/23    |08h  |
|                  |09h45| Mif26 Théorie des jeux
|                  |11h30|
|                  |14h  | Mif16 Techniques d'IA
|                  |15h45| Mif28 Logiciels éducatifs
|                  |17h30|
|Mardi 04/07/23    |08h  |
|                  |09h45|
|                  |11h30|

---

Toutes les informations sont sur le [site du M1 informatique](http://master-info.univ-lyon1.fr/M1/)

Réunion de rentrée : Lundi 5 septembre 2022 à 8h amphi Deperet [[slides]](rentree_M1_22-23.pdf) [[vidéo]](https://perso.liris.cnrs.fr/sylvain.brandel/doc/M1/rentree_M1_22-23.mp4)

Emplois-du-temps :
- Global [semestre d'automne](data/planning_M1_22-23_automne.pdf) - ATTENTION c'est indicatif, les cases blanches ne signifient pas forcément qu'il n'y a rien, seul le planning ADE ci-dessous fait foi
- Global [semestre de printemps](data/planning_M1_22-23_printemps.pdf) - ATTENTION c'est indicatif également
- Détaillé sur [ADE](https://adelb.univ-lyon1.fr/direct/index.jsp?projectId=2&ShowPianoWeeks=true&displayConfName=DOUA_CHBIO&showTree=false&resources=9767&days=0,1,2,3,4)
- Global [annuel](http://sylvain.brandel.pages.univ-lyon1.fr/m1/planning-alternance-2022-2023-v2.pdf) montrant les quinzaines alternants en formation et les quinzaines alternants en entreprise

---

## Présentation du master informatique

Mardi 14 mars 2023 11h30 amphi Thémis 8 - [slides](presentation_L3_vers_M1_22-23.pdf)

---

## Présentation des parcours M2

**mardi 29/11/2022 13h-14h salle Nautibus C2**

|                  |     |
|----------------- |-----|-----------
|Mardi 29/11/22    |13h  | M2 IA - Marie Lefevre
|                  |13h15| [M2 DISS](data/Pres_M2_DISS_2022.pdf) - Angela Bonifati
|                  |13h30| M2 TIW - Emmanuel Coquery / Fabien De Marchi
|Jeudi 01/12/22    |13h  | M2 DS - Alexandre Aussem
|                  |13h30| [M2 SRS](data/Pres_M2_SRS_2022.pdf) - Thomas Begin
|Vendredi 01/12/22 |13h15| M2 ID3D - Raphaëlle Chaine

---

## Présentation des options du M1 printemps

[Détail des UE et prérequis pour les M2](http://master-info.univ-lyon1.fr/M1/#12)

**Jeudi 01/12/2022 13h-14h salle Nautibus C2**

|     |                                                            |                         |Indispensable   |Complémentaire  |Plus
|-----|------------------------------------------------------------|-------------------------|----------------|----------------|----
|13h  | **M2 DS**                                                  | Alexandre Aussem        |                |                |
|13h10| M1if19 éval des perfs                                      | Alexandre Aussem        |                | DS - SRS - TIW | IA
|13h15| [M1if29 crypto et sécurité](data/Pres_Mif29_2022.pdf)           | Gérald Gavin            | SRS - TIW      |                |
|13h20| [M1if26 théorie des jeux](data/Pres_Mif26_2022.pdf)             | Gérald Gavin            | IA - DS        |                |
|13h25| [Mif32 prog fonctionnelle avancée](data/Pres_Mif32_2022.pdf)    | Xavier Urbain           |                |                |
|13h30| [**M2 SRS**](data/Pres_M2_SRS_2022.pdf)                         | Thomas Begin            |                |                |   
|13h40| [Mif20 projet transversal innovant](data/Pres_Mif20_2021.pdf)   | Erwan Guillou           |                |                |     


**Vendredi 02/12/2022 13h-14h salle Nautibus C2 aussi**

|     |                                                            |                         |Indispensable   |Complémentaire  |Plus
|-----|------------------------------------------------------------|-------------------------|----------------|----------------|----
|13h  | [Mif37 animation](http://alexandre.meyer.pages.univ-lyon1.fr/m1if37-animation)| Alexandre Meyer         | ID3D           |                |
|13h05| [Mif15 réseau par la pratique](data/Pres_Mif15_2022.pdf)        | Fabien Rico             | SRS            |                |
|13h10| [Mif14 BD déductives](data/Pres_Mif14_2022.pdf)                 | Angela Bonifati         | TIW - DISS     | DS - IA        |
|13h15| **M2 ID3D**                                                | Raphaelle Chaine        |                |                |
|13h25| Mif27 synthèse d'image                                     | Vincent Nivoliers       | ID3D           |                |
|13h30| Mif13 Web avancé [[page]](https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF13/)| Lionel Médini | TIW            | DS - IA        | ID3D
|13h35| [Mif28 logiciels éducatifs](data/Pres_Mif28_2022.pdf)           | Stéphanie Jean-Daubias  |                | IA             | ID3D
|13h40| [Mif16 techniques d'IA](data/Pres_Mif16_2022.pdf)               | Nadia Kabachi           | DS - IA - DISS | TIW            |
|13h45| [Mif22 parallélisme](data/Pres_Mif22_2022.pdf)                  | Laurent Lefevre         |                | SRS            | IA
|13h50| [Mif17 analyse d'image](data/Pres_Mif17_2021.pdf)               | Saida Bouakaz-Brondel   | ID3D - DISS    | IA             |


[Détail des UE et prérequis pour les M2](http://master-info.univ-lyon1.fr/M1/#12)

![Semestre de printemps](planning_M1_22-23_printemps.png)




## Offre de formation

Ce qui suit est un miroir technique du contenu du [site du M1 informatique](http://master-info.univ-lyon1.fr/M1/)


### Catalogue des UE proposées en M1

- [M1if01](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16765) (automne) - Gestion de projet et génie logiciel
- [M1if02](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=27574) (automne) - Informatique graphique et image
- [M1if03](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16767) (automne) - Conception d'applications web
- [M1if04](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26120) (automne) - Apprentissage et analyse de données
- [M1if05](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16769) (automne) - Réseaux
- [M1if06](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16770) (automne) - Bases de l'intelligence artificielle
- [M1if07](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26118) (automne) - Algorithmes pour l'optimisation
- [M1if08](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16772) (printemps) - Compilation / traduction des programmes
- [M1if09](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26119) (printemps) - Modèles de calcul et complexité
- [M1if10](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16774) (printemps) - Projet transversal de master informatique
- [M1if11](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26121) (automne) - Ouverture à la recherche
- [M1if12](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16776) (printemps) - Algorithmique distribuée
- [M1if13](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16777) (printemps) - Web avancé et web mobile
- [M1if14](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16778) (printemps) - Bases de données déductives
- [M1if15](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16779) (printemps) - Réseaux par la pratique
- [M1if16](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16780) (printemps) - Techniques de l'intelligence artificielle
- [M1if17](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16781) (printemps) - Analyse d'images
- [M1if18](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26123) (automne) - Systèmes avancés
- [M1if19](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16783) (printemps) - Évaluation des performances des systèmes
- [M1if20](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16784) (printemps) - Projet transversal innovant
- [M1if21](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26155) (annuel) - Stage alternant (18 ECTS)
- [M1if22](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16786) (printemps) - Parallélisme
- [M1if23](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16766) (automne) - Programmation avancée
- [M1if24](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26122) (automne) - Bases de données non relationnelles
- [M1if25](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16789) (automne) - Traitement du signal et communications numériques
- [M1if26](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16790) (printemps) - Théorie des jeux
- [M1if27](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16791) (printemps) - Synthèse d'image
- [M1if28](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16792) (printemps) - Logiciels éducatifs
- [M1if29](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16793) (printemps) - Cryptographie et sécurité
- [M1if30](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26124) (printemps) - Programmation d'applications mobile
- [M1if31](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=27575) (automne) - Insertion professionnelle
- [M1if32](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26125) (printemps) - Programmation fonctionnelle avancée
- [M1if33](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26126) (annuel) - Stage optionnel (0 ECTS)
- [M1if37](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16794) (printemps) - Animation en synthèse d'images
- [Anglais](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16764) (printemps) - Anglais pour la communication professionnelle

---

### Formation classique (hors contrat professionnel)

La formation comporte <b>5 blocs</b>.

##### BLOC <b>Calcul</b> (commun formations classique et en alternance)
- [M1if04](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26120) (automne) - Apprentissage et analyse de données
- [M1if07](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26118) (automne) - Algorithmes pour l'optimisation
- [M1if08](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16772) (printemps) - Compilation / traduction des programmes
- [M1if09](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26119) (printemps) - Modèles de calcul et complexité

##### BLOC <b>Réseau et programmation</b> (commun formations classique et en alternance)
- [M1if01](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16765) (automne) - Gestion de projet et génie logiciel
- [M1if03](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16767) (automne) - Conception d'applications web
- [M1if05](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16769) (automne) - Réseaux
- [M1if12](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16776) (printemps) - Algorithmique distribuée

##### BLOC <b>Thématique formation classique</b>
- [M1if02](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=27574) (automne) - Informatique graphique et image
- [M1if06](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16770) (automne) - Bases de l'intelligence artificielle
- [M1if23](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16766) (automne) - Programmation avancée

##### BLOC <b>Soft skills formation classique</b>
- [M1if11](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26121) (automne) - Ouverture à la recherche
- [M1if31](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=27575) (automne) - Insertion professionnelle
- [M1if10](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16774) (printemps) - Projet transversal de master informatique
- [Anglais](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16764) (printemps) - Anglais pour la communication professionnelle

##### BLOC <b>Options</b> (5 UE optionnelles au choix, ou 6 en cas de choix du stage optionnel)
- [M1if18](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26123) (automne) - Systèmes avancés
- [M1if24](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26122) (automne) - Bases de données non relationnelles
- [M1if25](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16789) (automne) - Traitement du signal et communications numériques
- [M1if13](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16777) (printemps) - Web avancé et web mobile
- [M1if14](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16778) (printemps) - Bases de données déductives
- [M1if15](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16779) (printemps) - Réseaux par la pratique
- [M1if16](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16780) (printemps) - Techniques de l'intelligence artificielle
- [M1if17](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16781) (printemps) - Analyse d'images
- [M1if19](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16783) (printemps) - Évaluation des performances des systèmes
- [M1if20](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16784) (printemps) - Projet transversal innovant
- [M1if22](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16786) (printemps) - Parallélisme
- [M1if26](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16790) (printemps) - Théorie des jeux
- [M1if27](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16791) (printemps) - Synthèse d'image
- [M1if28](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16792) (printemps) - Logiciels éducatifs
- [M1if29](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16793) (printemps) - Cryptographie et sécurité
- [M1if30](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26124) (printemps) - Programmation d'applications mobile
- [M1if32](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26125) (printemps) - Programmation fonctionnelle avancée
- [M1if37](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16794) (printemps) - Animation en synthèse d'images

Il faut <b>choisir ses options</b> en fonction du [M2 visé](http://master-info.univ-lyon1.fr/M1/#27).
<br>Afin de guider votre choix, voici les UE indispensables (1) / complémentaires (2) / qui seraient un plus (+) pour les différents parcours du Master Informatique de Lyon 1 :

######<b>[M2 DS](http://master-info.univ-lyon1.fr/DS/)</b> (Data Science)
- UE indispensables (1) :
  - M1if16 - Techniques d'IA
  - M1if26 - Théorie des jeux 
- UE complémentaires (2) :
  - M1if13 - Web avancé
  - M1if14 - BD déductives
  - M1if19 - Eval des perf

######<b>[M2 IA](http://master-info.univ-lyon1.fr/IA/)</b> (Intelligence Artificielle)
- UE indispensables (1) :
  - M1if14 - BD déductives
  - M1if16 - Techniques d’IA
  - M1if24 - BD non relationnelles
  - M1if26 - Théorie des jeux
- UE complémentaires (2) :
  - M1if13 - Web avancé
  - M1if17 - Analyse d’image
  - M1if28 - Logiciel éducatif
- UE qui seraient un plus (+) : 
  - M1if19 - Eval des Perf
  - M1if22 - Parallélisme

######<b>[M2 ID3D](http://master-info.univ-lyon1.fr/ID3D/)</b> (Image, Développement et Technologie 3D)
- UE indispensables (1) :
  - M1if17 - Analyse d’image
  - M1if27 - Synthèse d’image
  - M1if37 - Animation 
- UE complémentaires (2) :
  - M1if20 - Projet IDEFIX
  - M1if25 - Traitement du signal et communications numériques
- UE qui seraient un plus (+) :
  - M1if13 - Web avancé et Web Mobile
  - M1if28 - Logiciel éducatif

######<b>[M2 SRS](http://master-info.univ-lyon1.fr/SRS/)</b> (Systèmes, Réeaux et Sécurité)
- UE indispensables (1) :
  - M1if15 Réseaux pratique
  - M1if18 Systèmes avancés
  - M1if29 Cryptographie et sécurité
- UE complémentaires (2) :
  - M1if19 Eval de Perf
  - M1if22 Parallélisme
  - M1if25 Traitement signal et communications numériques

######<b>[M2 TIW](http://master-info.univ-lyon1.fr/TIW/)</b> (Technologies de l'Information et Web)
- UE indispensables (1) :
  - M1if13 Web avancé et web mobile
  - M1if14 BD déductives
  - M1if24 BD NoSQL
  - M1if29 Cryptographie et sécurité
- UE complémentaires (2) :
  - M1if16 Techniques d'IA
  - M1if19 Eval des perf

######<b>[M2 DISS](liris.cnrs.fr/angela.bonifati/)</b> (Data and Intelligence for Smart Systems)
- UE indispensables (1) :
  - M1if14 BD déductives
  - M1if16 Techniques d'IA
  - M1if17 Analyse d’image

######<b>[M2 IF](http://www.ens-lyon.fr/DI/informations-m2-20182019/)</b> (Informatique fondamentale)
- UE complémentaires (2) :
  - M1if12 Algorithmique distribuée
  - M1if19 Evaluation des performances des systèmes
  - M1if22 Parallélisme
  - M1if29 Cryptographie et sécurité



---

### Formation alternance TIW

La formation comporte <b>4 blocs</b>.

##### BLOC <b>Calcul</b> (commun formations classique et en alternance)
- [M1if04](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26120) (automne) - Apprentissage et analyse de données
- [M1if07](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26118) (automne) - Algorithmes pour l'optimisation
- [M1if08](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16772) (printemps) - Compilation / traduction des programmes
- [M1if09](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26119) (printemps) - Modèles de calcul et complexité

##### BLOC <b>Réseau et programmation</b> (commun formations classique et en alternance)
- [M1if01](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16765) (automne) - Gestion de projet et génie logiciel
- [M1if03](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16767) (automne) - Conception d'applications web
- [M1if05](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16769) (automne) - Réseaux
- [M1if12](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16776) (printemps) - Algorithmique distribuée

##### BLOC <b>Thématique alternance TIW</b>
- [M1if24](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26122) (automne) - Bases de données non relationnelles
- [M1if13](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16777) (printemps) - Web avancé et web mobile
- [M1if14](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16778) (printemps) - Bases de données déductives
- [M1if29](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16793) (printemps) - Cryptographie et sécurité

##### BLOC <b>Stage, anglais et ouverture à la recherche</b>
- [M1if11](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26121) (automne) - Ouverture à la recherche
- [Anglais](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16764) (printemps) - Anglais pour la communication professionnelle
- [M1if21](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26155) (annuel) - Stage alternant (18 ECTS)

L'anglais est Obligatoire - choix possible entre anglais pour la recherche et anglais pour l'entreprise.
Niveau B1 du [CECRL](http://eduscol.education.fr/cid45678/cadre-europeen-commun-de-reference.html) obligatoire (550 points au TOEIC) pour valider le M2.

---

### Formation alternance SRS

La formation comporte <b>4 blocs</b>.

##### BLOC <b>Calcul</b> (commun formations classique et en alternance)
- [M1if04](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26120) (automne) - Apprentissage et analyse de données
- [M1if07](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26118) (automne) - Algorithmes pour l'optimisation
- [M1if08](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16772) (printemps) - Compilation / traduction des programmes
- [M1if09](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26119) (printemps) - Modèles de calcul et complexité

##### BLOC <b>Réseau et programmation</b> (commun formations classique et en alternance)
- [M1if01](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16765) (automne) - Gestion de projet et génie logiciel
- [M1if03](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16767) (automne) - Conception d'applications web
- [M1if05](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16769) (automne) - Réseaux
- [M1if12](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16776) (printemps) - Algorithmique distribuée

##### BLOC <b>Thématique alternance SRS</b>
- [M1if18](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26123) (automne) - Systèmes avancés
- [M1if15](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16779) (printemps) - Réseaux par la pratique
- [M1if29](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16793) (printemps) - Cryptographie et sécurité
- Une UE à choisir parmi
  - [M1if25](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16789) (automne) - Traitement du signal et communications numériques
  - [M1if19](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16783) (printemps) - Évaluation des performances des systèmes
  - [M1if22](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16786) (printemps) - Parallélisme

##### BLOC <b>Stage, anglais et ouverture à la recherche</b>
- [M1if11](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26121) (automne) - Ouverture à la recherche
- [Anglais](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16764) (printemps) - Anglais pour la communication professionnelle
- [M1if21](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26155) (annuel) - Stage alternant (18 ECTS)

L'anglais est Obligatoire - choix possible entre anglais pour la recherche et anglais pour l'entreprise.
Niveau B1 du [CECRL](http://eduscol.education.fr/cid45678/cadre-europeen-commun-de-reference.html) obligatoire (550 points au TOEIC) pour valider le M2.

---

### Semestre d'automne

##### Pour les deux types de formation (classique et en alternance)
- [M1if01](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16765) - Gestion de projet et génie logiciel
- [M1if03](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16767) - Conception d'applications web
- [M1if04](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26120) - Apprentissage et analyse de données
- [M1if05](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16769) - Réseaux
- [M1if07](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26118) - Algorithmes pour l'optimisation
- [M1if11](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26121) - Ouverture à la recherche

##### Formation classique (hors contrat professionnel)
- [M1if02](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=27574) - Informatique graphique et image
- [M1if06](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16770) - Bases de l'intelligence artificielle
- [M1if23](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16766) - Programmation avancée
- [M1if31](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=27575) - Insertion professionnelle
- De 0 à 3 UE à choisir parmi (5 UE optionnelles à choisir sur toute l'année)
  - [M1if18](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26123) - Systèmes avancés
  - [M1if24](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26122) - Bases de données non relationnelles
  - [M1if25](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16789) - Traitement du signal et communications numériques

##### Formation alternance TIW
- [M1if24](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26122) - Bases de données non relationnelles

##### Formation alternance SRS
- [M1if18](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26123) - Systèmes avancés
- De 0 à 1 UE à choisir parmi (1 UE optionnelle à choisir sur toute l'année)
  - [M1if25](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16789) - Traitement du signal et communications numériques

---

### Semestre de printemps

##### Pour les deux types de formation (classique et en alternance)
- [M1if08](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16772) - Compilation / traduction des programmes
- [M1if09](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26119) - Modèles de calcul et complexité
- [M1if12](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16776) - Algorithmique distribuée
- [Anglais](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16764) - Anglais pour la communication professionnelle

##### Formation classique (hors contrat professionnel)
- [M1if10](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16774) - Projet transversal de master informatique
- De 2 à 5 UE à choisir parmi (5 UE optionnelles à choisir sur toute l'année)
  - [M1if13](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16777) - Web avancé et web mobile
  - [M1if14](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16778) - Bases de données déductives
  - [M1if15](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16779) - Réseaux par la pratique
  - [M1if16](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16780) - Techniques de l'intelligence artificielle
  - [M1if17](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16781) - Analyse d'images
  - [M1if19](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16783) - Évaluation des performances des systèmes
  - [M1if20](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16784) - Projet transversal innovant
  - [M1if22](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16786) - Parallélisme
  - [M1if26](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16790) - Théorie des jeux
  - [M1if27](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16791) - Synthèse d'image
  - [M1if28](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16792) - Logiciels éducatifs
  - [M1if29](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16793) - Cryptographie et sécurité
  - [M1if32](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=26125) - Programmation fonctionnelle avancée
  - [M1if37](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16794) - Animation en synthèse d'images

##### Formation alternance TIW
- [M1if13](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16777) - Web avancé et web mobile
- [M1if14](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16778) - Bases de données déductives

##### Formation alternance SRS
- [M1if15](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16779) - Réseaux par la pratique
- [M1if29](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16793) - Cryptographie et sécurité
- De 0 à 1 UE à choisir parmi (1 UE optionnelle à choisir sur toute l'année)
  - [M1if19](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16783) - Évaluation des performances des systèmes
  - [M1if22](http://offre-de-formations.univ-lyon1.fr/front_fiche_ue.php?UE_ID=16786) - Parallélisme

---

