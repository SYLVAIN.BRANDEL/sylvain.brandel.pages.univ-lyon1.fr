---
title: Anglais pour la communication professionnelle niveau 1
linkTitle: old/anglais23
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2023 - 2024**</span>

---

## Organisation

- 1 groupe d'anglais pour la recherche, 5 groupes d'anglais pour l'entreprise.
- Les enseignements d'anglais ont lieu pendant les semaines oranges - alternants présents.
- 8 séances de 3h par groupe.

![Planning global](planning_anglais_23-24.png)

Intervenants :
- Rech : Mme Mireille HUBER - [mireille.huber@univ-lyon1.fr](mailto:mireille.huber@univ-lyon1.fr)
- Ent1 : Mme Patricia MIRAMAND-GRAVAND - [patricia.miramand-gravrand@univ-lyon1.fr](mailto:patricia.miramand-gravrand@univ-lyon1.fr)
- Ent2 : M. Jacques PALESTRA - [jacques.palestra@univ-lyon1.fr](mailto:jacques.palestra@univ-lyon1.fr)
- Ent3 et Ent4 : M. Olivier BOUGUIN-VASILJEVIC - [olivier.bouguin-vasiljevic@univ-lyon1.fr](mailto:olivier.bouguin-vasiljevic@univ-lyon1.fr)
- Ent5 : M. Hervé JAMON - [herve.jamon@univ-lyon1.fr](mailto:herve.jamon@univ-lyon1.fr)



---


