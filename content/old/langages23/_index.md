---
title: LifLF - Langages formels
linkTitle: old/langages23
date: '2023-09-03'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2023 - 2024**</span>

---

## Planning global

![Planning global](planning-LF-23-24.png)


## Progression et ressources pédagogiques
|    |                                      |      |   | |
|----|------------- | -----------------------|--------------------------------------|---|---|
| 1 |lundi 04/09/23 | 9h45-11h15             |<span style="color: orange">CM0</span>|Introduction                             | [[slides]](LF-CM00.pdf)
|   |               |                        |<span style="color: orange">CM1</span>|Rappels maths                            | [[slides]](LF-CM01.pdf)
|   |mardi 05/09/23 | 14h-15h30              |<span style="color: orange">CM2</span>|[[Alphabets et langages                  | [[slides]](LF-CM02.pdf)
| 2 |lundi 11/09/23 | 9h45-11h15             |<span style="color: green"> TD1</span>|Ensembles et relations                   | [[sujet]](LF-TD1.pdf)
|   |mardi 12/09/23 | 14h-15h30              |<span style="color: orange">CM3</span>|Grammaires                               | [[slides]](LF-CM03.pdf)
| 3 |lundi 18/09/23 | 9h45-11h15             |<span style="color: green"> TD2</span>|Alphabets, langages, représentation finie| [[sujet]](LF-TD2.pdf)
|   |mardi 19/09/23 | 14h-15h30              |<span style="color: orange">CM4</span>|Automates à états finis                  | [[slides]](LF-CM04.pdf)
| 4 |lundi 25/09/23 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP1</span>|Prog. fonctionnelle en Coq / Gallina     | [[sujet]](LF-TP1.pdf) [[tp1.v]](tp/lf_tp1.v) [[tp1_cor.v]](tp/lf_tp1_correction_etu.v)
|   |mardi 26/09/23 | 14h-15h30              |<span style="color: orange">CM5</span>|Déterminisation                          | [[slides]](LF-CM05.pdf)
| 5 |lundi 02/10/23 | 8h-9h30                |<span style="color: green"> TD3</span>|Automates à états finis - déterminisation| [[sujet]](LF-TD3.pdf)|
|   |mardi 03/10/23 | 14h-15h30              |<span style="color: orange">CM6</span>|Caractérisation                          | [[slides]](LF-CM06.pdf)
| 6 |lundi 09/10/23 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP2</span>|Automates en Coq (part. 1)               | [[sujet]](LF-TP2.pdf) [[tp2.v]](tp/lf_tp2.v)  [[tp2_cor.v]](tp/lf_tp2_correction_etu.v)
|   |mardi 10/10/23 | 14h-15h30              |<span style="color: orange">CM7</span>|Minimisation des états                   | [[slides]](LF-CM07.pdf)
| 7 |lundi 16/10/23 | 8h-9h30                |<span style="color: green"> TD4</span>|Caractérisation - automate standard      | [[sujet]](LF-TD4.pdf)
|   |mardi 17/10/23 | 14h-15h30              |<span style="color: orange">CM8</span>|Langages rationnels - rationalité        | [[slides]](LF-CM08.pdf)
| 8 |lundi 23/10/23 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP3</span>|Automates en Coq (part. 2)               | [[sujet]](LF-TP3.pdf) [[tp3.v]](tp/lf_tp3.v) [[tp3_cor.v]](tp/lf_tp3_correction_etu.v)
|   |mardi 24/10/23 | 14h-15h30              |<span style="color: orange">CM9</span>|Automates à pile - algébricité           | [[slides]](LF-CM09.pdf)
|   |               |                        |                                      | Vacances universitaires
| 9 |lundi 06/11/23 | 8h-9h30                |<span style="color: green"> TD5</span>|Lemme d'Arden - rationalité              | [[sujet]](LF-TD5.pdf)
|   |mardi 07/11/23 | 14h-15h30              |<span style="color: orange">CMA</span>|Analyse ascendante                       | [[slides]](LF-CM10.pdf)
| 10|lundi 13/11/23 | 9h45-11h15             |<span style="color: green"> TD6</span>|Grammaires - analyse ascendante          | [[sujet]](LF-TD6.pdf)
| 11|lundi 20/11/23 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP4</span>|Grammaires et automates en Coq           | [[sujet]](LF-TP4.pdf) [[tp4.v]](tp/lf_tp4.v) <span style="color: gray">[tp4_cor.v]</span>
| 12|lundi 27/11/23 |                        |                                      | (pas d'enseignements de langages)
| 13|lundi 04/12/23 | 8h-13h                 |<span style="color: red">   TPN</span>|**TP noté**
|   |mardi 05/12/23 | 14h-15h30              |<span style="color: red">   ECA</span>|**Epreuve commune anonyme**


----


## Instructions pour les TP

  * On utilise Coq depuis une IDE (CoqIDE, VSCode, EMACS ...).

  * Lancez votre IDE :
    * **CoqIDE**, disponible sur les machines des salles de TP du campus (Linux uniquement)
    * VSCode, disponible sur les machines des salles de TP du campus, nécessite d'installer l'extension [VsCoq](https://github.com/coq-community/vscoq) avec la version 0.3.9
    * EMACS avec Proof General ...

  * Téléchargez le fichier .v (lien ci-dessus dans la progression), ouvrez-le dans votre IDE, lisez le sujet et complétez le fichier.v dans votre IDE.

Alternative à une IDE : [JsCoq](https://jscoq.github.io/scratchpad.html) ; Collez le sujet du TP dans le cadre de gauche

Pour voir correctement le sujet dans votre **navigateur**, configurez l'encodage par défaut de votre navigateur en Unicode (UTF-8)


## Pour travailler sur votre machine personnelle

### Pour installer CoqIDE sur votre machine

Les installeurs de CoqIDE incluent Coq.

  * MacOS :
      * Installeur binaire sur [https://github.com/coq/platform/releases/latest](https://github.com/coq/platform/releases/latest) (inclut CoqIDE)
      * Avec [Homebrew](https://brew.sh/index_fr) : `brew install coqide`

  * Windows : plusieurs manières d'installer CoqIDE, cf. [Installer Coq sous Windows](https://github.com/coq/coq/wiki/Installation%20of%20Coq%20on%20Windows) :
      * Installeur binaire sur [https://github.com/coq/platform/releases/latest](https://github.com/coq/platform/releases/latest) (inclut CoqIDE)
      * Installer Linux dans une VM ou dans WSL puis suivre les instructions pour Linux

  * Linux : cf. [Installer Coq sous Linux](https://github.com/coq/coq/wiki/Installation%20of%20Coq%20on%20Linux) (de préférence avec [opam](https://coq.inria.fr/opam-using.html))

### Pour utiliser Coq avec VSCode

  * Installez Coq (cf. ci-dessus)
  * Installez l'extension [VsCoq](https://github.com/coq-community/vscoq) dans VSCode, précisez version 0.3.9.


----

## Organisation

### Responsables

  * LifLF - Théorie des langages formels : Sylvain Brandel
  * LifLC - Logique classique : Xavier Urbain, [page pédagogique de logique classique](https://perso.liris.cnrs.fr/xavier.urbain/enseignement/)

### Volume

  * CM : 15 heures (10 x 1h30)
  * TD : 9 heures (6 x 1h30)
  * TP : 6 heures (4 x 1h30)

### Horaires

  * Tous les enseignements de LifLF (et LifLC) ont lieu dans la séquence 1 (lundi matin et mardi après-midi)
  * Globalement, les CM ont lieu le mardi après-midi de 14h à 15h30, les TD / TP le lundi matin de 8h à 9h30 et de 9h45 à 11h15 ou de 11h30 à 13h ; chaque créneau de 8h à 9h30 et de 9h45 à 11h15 ou de 11h3à à 13h est occupé soit par un TD / TP de théorie des langages, soit par un TD / TP de logique classique
  * À partir du 25 septembre 2023
      * **TD** de LC **ou** de LF de 8h à 9h30
      * **TP** de LC **ou** de LF de 9h45 à 11h15 **ou** de 11h30 à 13h
  * Fiez-vous à votre emploi du temps sur ADE

### Intervenants

  * Sylvain Brandel (CM, TD et TP groupe A)
  * Emmanuel Coquery (TD et TP groupe B)
  * Fabien De Marchi (TD et TP groupe C)
  * Xavier Urbain (TD et TP groupe D)
  * Mohand-Said Hacid (TD groupe E)
  * Romuald Thion (TP groupe E

### Evaluation

  * **Contrôle continu intégral**  :
      * TP noté lundi 4 décembre 2023 8h ou 9h45
      * ECA mardi 5 décembre 2023 14h
      * Interros en début de TD, peut-être un autre TP noté …
      * Rattrapage en juin 2024
  * **Règles d'absences aux contrôles de TD** :
    * Zéro en cas d'absence à un contrôle de TD, justifiée ou non, quelle qu'en soit la raison
    * Pas de rattrapage des contrôles en cas d'absence (légère tolérence au retard)
    * La moyenne des n contrôles sera calculée à partir des (n-1) meilleures notes
    * Le jury d'UE discutera des cas particuliers (absence longue durée, absences répétées ...)

----



