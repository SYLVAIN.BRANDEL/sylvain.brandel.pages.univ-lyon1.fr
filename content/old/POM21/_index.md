---
title: Mif11 - Projet d'orientation en master
linkTitle: old/POM21
date: '2021-11-06'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2021 - 2022**</span>

---

<span style="color: red">**IMPORTANT** : vous présenterez votre travail lors d'une soutenance jeudi 16 juin 2022 après-midi</span>

----

## Planning des soutenances du jeudi 16 juin 2022

<span style="color: red">PLANNING DEFINITIF</span> (à quelques ajustements potentiels près)

### Jury 1 - Salle Nautibus TD3

|       | Juré 1 | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 |
| ----- | ------ | ------ | ----- | ---------- | ---------- |
| 13h00 | I. Guérin-Lassous | T. Begin | 1 | T. MODRZYK	|		|
| 13h30 | I. Guérin-Lassous | T. Begin | 2 | C. ARALOU	|	N. VALLOT	|
| 14h00 | I. Guérin-Lassous | H. Kheddouci | 3 | I. GRAND	|		|
| 14h30 | I. Guérin-Lassous | H. Kheddouci | 4 | C. LOUIS-ITTY	|		|
| 15h00 | I. Guérin-Lassous | H. Kheddouci | 5 | S. BEKKIS	|		|
| 15h30 | I. Guérin-Lassous | H. Kheddouci | 6 | L. FOURNIER	|		|
| 16h00 | I. Guérin-Lassous | H. Kheddouci | 7 | A. COMO	|		|
| 16h30 | I. Guérin-Lassous | H. Kheddouci | 8 | C. CROZ	|		|
| 17h00 | I. Guérin-Lassous | H. Kheddouci | 9 | M. GOMEZ	|		|
| 17h30 | I. Guérin-Lassous | H. Kheddouci | 10 | A. BOURGUIN	|		|

### Jury 2 - Salle Nautibus TD9

|       | Juré 1 | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 |
| ----- | ------ | ------ | ----- | ---------- | ---------- |
| 13h00 | E. Galin | S. Aknine | 11 | R. LEMAITRE	|		|
| 13h30 | E. Galin | S. Aknine | 12 | J. AL HAYEK	|	W. BELDJA	|
| 14h00 | S. Jean-Daubias | S. Aknine | 13 | Q. CHAPEL	|		|
| 14h30 | S. Jean-Daubias | S. Aknine | 14 | N. CHAMAND	|		|
| 15h00 | S. Jean-Daubias | S. Aknine | 15 | F. LOMBARD	|		|
| 15h30 | S. Jean-Daubias | S. Aknine | 16 | C. MARREL	|	F. SAID ALI	|
| 16h00 | S. Jean-Daubias | R. Cazabet | 17 | A. BENALI	|	F. PAYEN	|
| 16h30 | S. Jean-Daubias | R. Cazabet | 18 | H. DJEGHRI	|		|
| 17h00 | S. Jean-Daubias | R. Cazabet | 19 | A. OSTER	|	C. ZAGALA	|
| 17h30 | S. Jean-Daubias | R. Cazabet | 20 | O. BRUNEAU	|	J. GONCALVES	|

### Jury 3 - Salle Nautibus TD10

|       | Juré 1 | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 |
| ----- | ------ | ------ | ----- | ---------- | ---------- |
| 13h00 | A. Tabard | E. Coquery | 31 | A. BARRY	|	H. TON	|
| 13h30 | A. Tabard | E. Coquery | 32 | M. BRUSTOLIN	|	T. OGIER	|
| 14h00 | A. Tabard | E. Coquery | 33 | G. LE GURUN	|	M. NESR	|
| 14h30 | A. Tabard | E. Coquery | 34 | A. ROMAN	|		|
| 15h00 | A. Tabard | A. Meyer | 35 | J. BRESSAT	|	G. COGONI	|
| 15h30 | A. Tabard | A. Meyer | 36 | T. MONTERO	|		|
| 16h00 | A. Tabard | A. Meyer | 37 | A. ABBOUD	|	O. CHARLERY	|
| 16h30 | A. Tabard | A. Meyer | 38 | M. BOGADO GARCIA	|		|
| 17h00 | G. Damiand | A. Meyer | 39 | A. DURY	|		|
| 17h30 | G. Damiand | A. Meyer | 40 | IL. SMAIL	|	E. LAFOURCADE	|

### Jury 4 - Salle Nautibus TD11

|       | Juré 1 | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 |
| ----- | ------ | ------ | ----- | ---------- | ---------- |
| 13h00 | Ch. Crespelle | Y. Caniou | 52 | A. VALETTE	|		|
| 13h30 | Ch. Crespelle | Y. Caniou | 53 | C. LUSSIEZ	|		|
| 14h00 | Ch. Crespelle | Y. Caniou | 54 | N. MOHAMED MAHAMOUD	|		|
| 14h30 | Ch. Crespelle | Y. Caniou | 55 | H. WANG	|		|
| 15h00 | Ch. Crespelle | G. Radanne | 56 | G. YE	|		|
| 15h30 | Ch. Crespelle | G. Radanne | 57 | D. DEMEERSSEMAN	|		|
| 16h00 | Ch. Crespelle | G. Radanne | 59 | M. EL HADJ MUSTAPHA	|		|
| 16h30 | Ch. Crespelle | G. Radanne | 60 | B. BOUDJEMA	|		|
| 17h00 |  | | | | |
| 17h30 |  | | | | |

### Jury 5 - Salle Nautibus TD12

|       | Juré 1 | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 |
| ----- | ------ | ------ | ----- | ---------- | ---------- |
| 13h00 | N. Louvet | J. Digne | 41 | N. ABDULLATIEF	|	M. EL AZZOUZI	|
| 13h30 | N. Louvet | J. Digne | 42 | I. KORICHI	|	R. SAID	|
| 14h00 | E. Guillou | V. Nivoliers | 43 | E. DJEDOUI	|		|
| 14h30 | E. Guillou | V. Nivoliers | 45 | Y. EL HIRACH	|		|
| 15h00 | E. Guillou | F. Jaillet | 46 | T. GUILLARDEL	|	H. POPOFF	|
| 15h30 | E. Guillou | F. Jaillet | 47 | R. FORESTIER	|		|
| 16h00 | E. Guillou | F. Jaillet | 48 | A. PRADIER	|		|
| 16h30 | E. Guillou | F. Jaillet | 49 | M. BERTOLONE--LOPEZ-SERRANO	|		|
| 17h00 | E. Guillou | F. Jaillet | 50 | J. DE ALMEIDA	|		|
| 17h30 | E. Guillou | F. Jaillet | 51 | T. HERVIER	|		|

### Jury 6 - Salle Nautibus TD13

|       | Juré 1 | Juré 2 | IdGrp | Etudiant 1 | Etudiant 2 |
| ----- | ------ | ------ | ----- | ---------- | ---------- |
| 13h00 | F. Duchateau | H. Ladjal | 21 | S. KLOPFENSTEIN	|	M. MBOUP	|
| 13h30 | F. Duchateau | H. Ladjal | 23 | D. LOREK	|	D. RANCHON	|
| 14h00 | F. Duchateau | H. Ladjal | 24 | G. DELOIN	|	T. TRACOL	|
| 14h30 | F. Duchateau | H. Ladjal | 25 | Y. ELGHAZI	|	Y. MERZOUGUI	|
| 15h00 | F. Duchateau | N. Guin | 26 | J. CENDRIER	|		|
| 15h30 | F. Duchateau | N. Guin | 22 | V. DUPRIEZ	|	C. MOKHTARI	|
| 16h00 | F. Duchateau | MS. Hacid | 27 | F. DIET	|		|
| 16h30 | F. Duchateau | MS. Hacid | 28 | J. ROSSI	|		|
| 17h00 | X. Urbain | MS. Hacid | 29 | J. MILLET	|		|
| 17h30 | X. Urbain | MS. Hacid | 30 | J. HUBERT	|	S. NARET	|

----

## Objectif

L'objectif de cette UE est guider les étudiants dans leurs choix professionnels, qui commencent, pour les étudiants de M1, par le choix de leur futur M2 et bien souvent par le choix d’une orientation entre industrie ou recherche.

Cette UE obligatoire vaut 6 crédits et correspond à un travail personnel conséquent de la part de l'étudiant : un projet courant de fin octobre 2021 à juin 2022.


----

## Déroulement

* Dépôt des sujets (pour les enseignants) :
  * Date limite : 18 octobre 2021
  * Sur la page : [M1POM-enseignants](https://tomuss.univ-lyon1.fr/2021/Public/M1POM)

* Présentation de l'UE
  * Mercredi 20 octobre 2021 de 11h30 à 13h, Amphi Thémis 7
  * [Présentation](mif11.pdf)

* Choix des sujets :
  * Date limite : 25 octobre 2021
  * Liste des sujets : [M1POM-etu](https://tomuss.univ-lyon1.fr/S/2020/Automne/public_login/2021/Public/M1POM)

* Projet de recherche :
  * Du 25 octobre 2021 (dès le choix du sujet) à mai 2021,
  * Prendre RDV avec votre encadrant à l'avance à partir du 25 octobre pour le premier jour

* Cahier des charges :
  * Date limite : **15 novembre 2021**
  * Rédaction en interaction avec vos encadrants à partir de novembre
  * 4 pages maximum tout compris, [exemple 1](mif11_excdc1.pdf), [exemple 2](mif11_excdc2.pdf)
  * Fichier au format .pdf intitulé cdc_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro de groupe apparaissant sur Tomuss
  * À déposer sur Tomuss, dans l'UE UE-INF1097M Projet Pour l'Orientation en Master

* Rapport final :
  * Date limite : **Jeudi 9 juin 2022**
  * Un rapport par binôme
  * 10 pages maximum (pas de page de garde, pas de plan), annexes autorisées (les annexes ne comptent pas dans les 10 pages), marges de 2cm, police Calibri (ou équivalente) 11pt, [modèle](mif11_modele.pdf) à respecter mais des adaptations sont possibles (Latex, etc.), [exemple1](mif11_exr1.pdf), [exemple2](mif11_exr2.pdf)
    * Fichier au format .pdf intitulé rapport_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf où IDgrp est votre numéro d groupe apparaissant sur Tomuss,
    * À déposer sur Tomuss, dans l'UE UE-INF1097M Projet Pour l'Orientation en Master
    * Plutôt que déposer un pdf, vous pouvez indiquer une URL où trouver votre rapport

* <span style="color: red">**Soutenance**</span> :
  * **Jeudi 16 juin 2022 après-midi** (planning à suivre)
  * Une soutenance par binôme
  * 15 minutes de présentation + 5 minutes de questions. <span style="color: red">Respectez bien le timing</span>, la note de soutenance en tiendra compte
  * La soutenance devra mettre en évidence la problématique de votre projet et votre contribution
  * Si vous êtes à deux (ou trois), veillez à vous répartir équitablement lors de la présentation
  * Vous pourrez faire une démo, dans le temps imparti de la présentation

* ~~Poster~~ :
    * ~~Date limite : (à préciser)~~
    * ~~Un poster format par binôme~~
    * ~~Fichier A1 au format .pdf intitulé poster_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf~~
    * ~~À déposer sur Tomuss~~
    * ~~L'impression des posters non déposés dans les temps sera à la charge des étudiants~~

* ~~Présentation des posters~~ :
  * ~~(date à préciser)~~
  * ~~Présentation de vos travaux autours de votre poster entre 14 et 18h~~
  * ~~Vous pouvez également utiliser votre ordinateur pour faire des démos~~

* Video de vulgarisation de votre travail :
  * Date limite : **Vendredi 17 juin 2022**
  * 3 minutes maximum
  * À déposer sur Tomuss
  * Fichier intitulé video_IDgrp_nom1_numetudiant1_nom2_numetudiant.format avec format lisible au moins avec VLC
  * La vidéo ne doit pas être une soutenance bis, mais permettre à quelqu'un qui n'est pas du domaine de comprendre votre travail

* Note du travail (pour les enseignants) : Mardi 21 juin 2022

* Barème indicatif :
  * Note du travail évalué par l'encadrant : coef. 3
  * Note du rapport évalué par deux rapporteurs : coef. 3 (1.5 par rapporteur)
  * Note de la soutenance évaluée par le jury de soutenance : coef. 3
  * Note de la vidéo de vulgarisation : coef. 1

----

## Précisions et évaluation

* Organisation pour le choix des sujets : vous consultez les sujets disponibles en ligne, puis contactez les responsables des sujets qui vous intéressent. Les sujets peuvent être traités seul ou en binôme. Les binômes peuvent être constitués par les étudiants eux-mêmes ou proposés par les enseignants.
* Remarque générale : vous apprenez à mener à bien un projet (de recherche ou de développement), vous apprenez aussi à présenter votre travail, d'où l'importance du respect des consignes.
* Conseil pour tous les écrits : attention à l'expression, l'orthographe, la grammaire, la conjugaison, la ponctuation …


