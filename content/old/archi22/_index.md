---
title: ARCHI - Architecture des ordinateurs
linkTitle: old/archi22
date: '2021-11-30'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## Progression et ressources pédagogiques

| | | | | |
| --- | --- | --- | --- | --- |
| 1 | mardi 06/09/22 |               |                                     | (Pas d'enseignements d'architecture)
|   | jeudi 08/09/22 | 14h-15h30     |<span style="color: orange">CM0</span>| [[Introduction]](ARCHI-CM00.pdf)
|   |                |               |<span style="color: orange">CM1</span>| [[Vue d'ensemble de l'ordinateur]](ARCHI-CM01.pdf)
|   | jeudi 08/09/22 | 15h45-17h15   |<span style="color: orange">CM2</span>| [[Codage des données en machine part. 1]](ARCHI-CM02.pdf) - codage des entiers
| 2 | mardi 13/09/22 | 8h-11h15      |<span style="color: green"> TD1</span>| Vue d'ensemble de l'ordinateur (sujet [ci-dessous](#exercices-de-td-et-tp))
|   | jeudi 15/09/22 | 14h-15h30     |<span style="color: orange">CM3</span>| [[Codage des données en machine part. 2]](ARCHI-CM03.pdf) - codage des rationnels
|   | jeudi 15/09/22 | 15h45-17h15   |<span style="color: orange">CM4</span>| [[Logique propositionnelle]](ARCHI-CM04.pdf)
| 3 | mardi 20/09/22 | 8h-11h15      |<span style="color: green"> TD2</span> | Codage des nombres en machine 
|   | jeudi 22/09/22 | 14h-15h30     |<span style="color: orange">CM5</span>| [[Circuits combinatoires part. 1]](ARCHI-CM05.pdf) - logique anarchique et structurée [[Pulley.mp4]](Pulley-Logic-Gates-HD.mp4)
| 4 | mardi 27/09/22 | 8h-11h15      |<span style="color: purple">TP1</span>| Circuits combinatoires part. 1 [[sujet]](ARCHI-TP1.pdf) [[comp2etu.c]](tp/tp1_comp2etu.c)
|   | jeudi 29/09/22 | 14h-15h30     |<span style="color: orange">CM6</span>| [[Circuits combinatoires part. 2]](ARCHI-CM06.pdf) - logique en tranche
| 5 | mardi 04/10/22 | 8h-11h15      |<span style="color: green"> TD3</span>| Logique propositionnelle
|   | jeudi 06/10/22 | 14h-15h30     |<span style="color: orange">CM7</span>| [[Circuits séquentiels part. 1]](ARCHI-CM07.pdf) - bascules [[démo logisim]](sequentiel.circ)
| 6 | mardi 11/10/22 | 8h-11h15      |<span style="color: purple">TP2</span>| Circuits combinatoires part. 2 [[sujet]](ARCHI-TP2.pdf) [[aluetu.circ]](tp/tp2_aluetu.circ) [[pgcdetu.circ]](tp/tp2_pgcdetu.circ)
|   | jeudi 13/10/22 | 14h-15h30     |<span style="color: orange">CM8</span>| [[Circuits séquentiels part. 2]](ARCHI-CM08.pdf) - compteurs, automates, registres, RAM
| 7 | mardi 18/10/22 | 8h-11h15      |<span style="color: green"> TD4</span>| Circuits combinatoires
|   | jeudi 20/10/22 | 14h-15h30     |<span style="color: orange">CM9</span>| [[Langage d'assemblage du LC-3 part. 1]](ARCHI-CM09.pdf) - instructions A&L, mémoire
| 8 | mardi 25/10/22 | 8h-11h15      |<span style="color: purple">TP3</span>| Circuits séquentiels [[sujet]](ARCHI-TP3.pdf)
|   | jeudi 27/10/22 | 14h-15h30     |<span style="color: orange">CM10</span>| [[Langage d'assemblage du LC-3 part. 2]](ARCHI-CM10.pdf) - routines et pile
|   |                |               |                                       | (Vacances universitaires)
|   |                |               |                                       |
|   |                |               |                                       | <span style="color: red">**SEMAINES 9 A 13 POUR LE GROUPE A**</span>
|   |                |               |                                       |
| 9 | mardi 08/11/22 | 8h-11h15      |<span style="color: green"> TD5</span>| Circuits séquentiels
|   | mardi 08/11/22 | <span style="color: red">11h30-13h</span> |<span style="color: purple">TP4</span>| Programmation LC-3 part. 1 [[sujet]](ARCHI-TP4.pdf)<br/> [[add_simple.asm]](tp/tp4_add_simple.asm) [[puts.asm]](tp/tp4_puts.asm) [[bin.asm]](tp/tp4_bin.asm) [[puts2.asm]](tp/tp4_puts2.asm)  [[loop.asm]](tp/tp4_loop.asm)  [[lc3os.asm]](tp/lc3os.asm)
|   | jeudi 10/11/22 | 14h-15h30     |<span style="color: orange">CM11</span>| [[Ébauche d'un processeur]](ARCHI-CM11.pdf)
| 10| mardi 15/11/22 | <span style="color: red">8h-9h30</span> |<span style="color: purple">TP4</span>|  Programmation LC-3 part. 1 (suite)
|   | mardi 15/11/22 | <span style="color: red">9h45-13h</span> |<span style="color: purple">TP5</span>| Programmation LC-3 part. 2 [[sujet]](ARCHI-TP5.pdf) <br/>[[mult6.asm]](tp/tp5_mult6.asm) [[codage.asm]](tp/tp5_codage.asm) [[chaine.asm]](tp/tp5_chaine.asm) [[saisie.asm]](tp/tp5_saisie.asm)
|   | jeudi 17/11/22 | 14h-15h30     |<span style="color: orange">CM12</span>| [[Mémoire cache et pipeline]](ARCHI-CM12.pdf)
| 11| mardi 22/11/22 | 8h-11h15      |<span style="color: green"> TD6</span>| Programmation en langage d'assemblage
|   | mardi 22/11/22 | <span style="color: red">11h30-13h</span> |<span style="color: purple">TP6</span>| Implémentation du LC-3 part. 1 [[sujet]](ARCHI-TP6.pdf) [[LC3_etu.circ]](tp/LC3_etu.circ) [[add_simple.mem]](tp/tp6_add_simple.mem)
| 12| mardi 29/11/22 | <span style="color: red">8h-9h30</span>|<span style="color: purple">TP6</span>| Implémentation du LC-3 part. 1 (suite)
|   | mardi 29/11/22 | <span style="color: red">9h45-13h</span> |<span style="color: purple">TP7</span>| Implémentation du LC-3 part. 2 [[sujet]](ARCHI-TP7.pdf)
| 13| mardi 06/12/22 | 8h-9h30       | ÉCA  | **Épreuve Commune Anonyme**|
|   | mardi 06/12/22 | 9h45-13h      | <span style="color: purple">TP8</span>  | **TP noté**|
|   |                |               |                                       |
|   |                |               |                                       | <span style="color: red">**SEMAINES 9 A 13 POUR LES GROUPES B, C, D, E**</span>
|   |                |               |                                       |
| 9 | mardi 08/11/22 | 8h-11h15      |<span style="color: green"> TD5</span>| Circuits séquentiels
|   | jeudi 10/11/22 | 15h45-17h15   |<span style="color: orange">CM11</span>| [[Ébauche d'un processeur]](ARCHI-CM11.pdf)
| 10| mardi 15/11/22 | 8h-11h15      |<span style="color: purple">TP4</span>|  Programmation LC-3 part. 1 [[sujet]](ARCHI-TP4.pdf)<br/> [[add_simple.asm]](tp/tp4_add_simple.asm) [[puts.asm]](tp/tp4_puts.asm) [[bin.asm]](tp/tp4_bin.asm) [[puts2.asm]](tp/tp4_puts2.asm)  [[loop.asm]](tp/tp4_loop.asm)  [[lc3os.asm]](tp/lc3os.asm) 
|   | jeudi 17/11/22 | 14h-15h30     |<span style="color: orange">CM12</span>| [[Mémoire cache et pipeline]](ARCHI-CM12.pdf)
| 11| mardi 22/11/22 | 8h-11h15      |<span style="color: green"> TD6</span>| Programmation en langage d'assemblage
|   | <span style="color: red">jeudi 24/11/22</span> | <span style="color: red">14h-17h15</span> |<span style="color: purple">TP5</span>| Programmation LC-3 part. 2 [[sujet]](ARCHI-TP5.pdf) <br/>[[mult6.asm]](tp/tp5_mult6.asm) [[codage.asm]](tp/tp5_codage.asm) [[chaine.asm]](tp/tp5_chaine.asm) [[saisie.asm]](tp/tp5_saisie.asm) 
| 12| mardi 29/11/22 | 8h-11h15      |<span style="color: purple">TP6</span>| Implémentation du LC-3 part. 1 [[sujet]](ARCHI-TP6.pdf) [[LC3_etu.circ]](tp/LC3_etu.circ) [[add_simple.mem]](tp/tp6_add_simple.mem)
|   | <span style="color: red">jeudi 01/12/22</span> | <span style="color: red">14h-17h15</span> |<span style="color: purple">TP7</span>| Implémentation du LC-3 part. 2 [[sujet]](ARCHI-TP7.pdf)
| 13| mardi 06/12/22 | 8h-9h30       | ÉCA  | **Épreuve Commune Anonyme**|
|   | mardi 06/12/22 | 9h45-13h      | <span style="color: purple">TP8</span>  | **TP noté**|


### Ressources complémentaires

  * TD [[Exercices de TD]](ARCHI-TD.pdf) (un exemplaire papier est distribué lors de la première séance de TD)
  * TP [[Mémo de TP]](ARCHI-TP-memo.pdf), [[Logisim]](logisim-generic-2.7.1.jar), [[Pensim]](PennSim.jar)

  * [Exercices interactifs](https://perso.liris.cnrs.fr/vincent.nivoliers/suzette.php?exo=bintable&query=create&ticket=demain&html=true) proposés par Vincent Nivoliers

----

## Organisation

### Responsable

  * ARCHI - Architecture des ordinateurs : Sylvain Brandel

### Volume

  * CM : 18 heures (12 x 1h30)
  * TD : 18 heures (6 x 3h)
  * TP : 24 heures (8 x 3h)

### Horaires

  * Tous les enseignements de ARCHI ont lieu dans la séquence 3 (mardi matin et jeudi après-midi)
  * Globalement, les CM ont lieu le jeudi après-midi de 14h à 15h30, les TD / TP le mardi matin de 8h à 11h15

### Intervenants

| | | | |
| --- | --- | --- | --- |
|Sylvain Brandel   |CM|TD gr. A|TP gr. A1|
|                  |  |        |TP gr. A2|
|Vincent Nivoliers |  |TD gr. B|TP gr. B1|
|                  |  |        |TP gr. B2|
|Xavier Urbain     |  |TD gr. C|TP gr. C1|
|                  |  |        |TP gr. C2|
|Qi Qiu            |  |TD gr. D|TP gr. D1|
|Amaury Maille     |  |        |TP gr. D2|
|Brice-Edine Bellon|  |TD gr. E|TP gr. E1|
|Franck Favetta    |  |        |TP gr. E2|

### Evaluation

  * **Contrôle continu intégral**  :
      * ÉCA mardi 6 décembre 2022 à 8h
      * TP noté mardi 6 décembre 2022 à partir de 9h45
      * Interros en début de TD, peut-être un autre TP noté ...

---

## Annales

  * [ÉCA 2019](annales/LifASR4-ECA-2019.pdf)
  * [ÉCA 2020](annales/LifASR4-ECA-2020.pdf)

  * [TP noté 2020](annales/LifASR4-TPNOTE-2020.pdf)

