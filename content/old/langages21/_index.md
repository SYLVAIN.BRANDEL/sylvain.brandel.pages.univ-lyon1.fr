---
title: LifLF - Langages formels
linkTitle: old/langages21
date: '2021-11-05'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2021 - 2022**</span>

---

## Progression et ressources pédagogiques
|    |                                      |      |   |
|----|--------------------------------------|----- |---|
| S1 |lundi 06/09/21 9h45-11h15             |**CM**|[CM0](LifLF-CM00.pdf) Introduction - [CM1](LifLF-CM01.pdf) Rappels maths|
|    |mardi 07/09/21 14h-15h30              |**CM**|[CM2](LifLF-CM02.pdf)  Alphabets et langages|
| S2 |lundi 13/09/21 9h45-11h15             |  TD  |[TD1](LifLF-TD1.pdf) Ensembles et relations|
|    |lundi 13/09/21 11h30-13h              |  TD  |[TD2](LifLF-TD2.pdf) Alphabets, langages, représentation finie|
|    |mardi 14/09/21 14h-15h30              |**CM**|[CM3](LifLF-CM03.pdf)  Grammaires|
| S3 |lundi 20/09/21 9h45-11h15 ou 11h30-13h|  TP  |[TP1](LIFLF_TP1.v) [TP1_corr](LIFLF_TP1_correction_etu.v) Prog. fonctionnelle en Coq / Gallina|
|    |mardi 21/09/21 14h-15h30              |**CM**|[CM4](LifLF-CM04.pdf) Automates à états finis|
| S4 |lundi 27/09/21 9h45-11h15 ou 11h30-13h|  TP  |[TP2](LIFLF_TP2.v) [TP2_corr](LIFLF_TP2_correction_etu.v) Automates en Coq (part. 1)|
|    |mardi 28/09/21 14h-15h30              |**CM**|[CM5](LifLF-CM05.pdf) Déterminisation|
| S5 |lundi 04/10/21 8h-9h30                |  TD  |[TD3](LifLF-TD3.pdf) Automates à états finis - déterminisation|
|    |mardi 05/10/21 14h-15h30              |**CM**|[CM6](LifLF-CM06.pdf) Caractérisation|
| S6 |lundi 11/10/21 9h45-11h15 ou 11h30-13h|  TP  |[TP3](LIFLF_TP3.v) [TP3_corr](LIFLF_TP3_correction_etu.v) Automates en Coq (part. 2)|
|    |mardi 12/10/21 14h-15h30              |**CM**|[CM7](LifLF-CM07.pdf) Minimisation des états|
| S7 |lundi 18/10/21 8h-9h30                |  TD  |[TD4](LifLF-TD4.pdf) Caractérisation - automate standard|
|    |mardi 19/10/21 14h-15h30              |**CM**|[CM8](LifLF-CM08.pdf) Langages rationnels - rationalité|
|    |                                      |      | Vacances universitaires  |
| S8 |lundi 01/11/21                        |      | (lundi férié)  |
|    |mardi 02/11/21 14h-15h30              |**CM**|[CM9](LifLF-CM09.pdf) Automates à pile - algébricité|@
| S9 |lundi 08/11/21 8h-9h30                |  TD  |[TD5](LifLF-TD5.pdf) Lemme d'Arden - rationalité|
|    |mardi 09/11/21 14h-15h30              |**CM**|[CM10](LifLF-CM10.pdf) Analyse syntaxique|
| S10|lundi 15/11/21 9h45-11h15 ou 11h30-13h|  TP  |[TP4](LIFLF_TP4.v) [TP4_corr](LIFLF_TP4_correction_etu.v) Grammaires et automates en Coq|
| S11|lundi 22/11/21 8h-9h30                |  TD  |[TD6](LifLF-TD6.pdf) Grammaires - analyse ascendante|
| S12|lundi 29/11/21                        |      | (pas d'enseignements de langages formels cette semaine)  |
| S13|lundi 06/12/21 8h-11h15               |<span style="color: red">TPN</span>|TP noté|
|    |mardi 07/12/21 8h-9h30                |<span style="color: red">ÉCA</span>|Épreuve commune anonyme|
| S14|lundi 13/12/21                        |      | |

----



## Instructions pour les TP

  * Téléchargez le sujet (lien ci-dessus dans la progression), ouvrez-le dans CoqIDE, lisez-le et complétez-le.

On parle bien de **CoqIDE**, c'est-à-dire l'interface graphique permettant d'invoquer Coq. Installer Coq tout court ne vous servira pas à grand chose. Les installeurs de CoqIDE incluent Coq.

  * Si CoqIDE non disponible : [JsCoq](https://jscoq.github.io/scratchpad.html) Collez le sujet du TP dans le cadre de gauche

Pour voir correctement le sujet dans votre **navigateur**, configurez l'encodage par défaut de votre navigateur en Unicode (UTF-8)

Pour installer CoqIDE sur votre machine :

  * MacOS :
      * Installeur binaire sur [https://github.com/coq/coq/releases/tag/V8.13.2](https://github.com/coq/coq/releases/tag/V8.13.2] (inclut CoqIDE)
      * Avec [Homebrew](https://brew.sh/index_fr) : `brew install coqide`

  * Windows : plusieurs manières d'installer CoqIDE, cf. [Installer Coq sous Windows](https://github.com/coq/coq/wiki/Installation%20of%20Coq%20on%20Windows) :
      * Installeur binaire sur [https://github.com/coq/coq/releases/tag/V8.13.2](https://github.com/coq/coq/releases/tag/V8.13.2) (inclut CoqIDE)
      * Installer Linux dans une VM ou dans WSL puis suivre les instructions pour Linux
  * Linux : cf. [Installer Coq sous Linux](https://github.com/coq/coq/wiki/Installation%20of%20Coq%20on%20Linux)

----

## Organisation

### Responsables

  * LifLF - Théorie des langages formels : Sylvain Brandel
  * LifLC - Logique classique : Xavier Urbain, [page pédagogique de logique classique](https://perso.liris.cnrs.fr/xavier.urbain/ens/LIFLC/)

### Volume

  * CM : 15 heures (10 x 1h30)
  * TD : 9 heures (6 x 1h30)
  * TP : 6 heures (4 x 1h30)

### Horaires

  * Tous les enseignements de LifLF (et LifLC) ont lieu dans la séquence 1 (lundi matin et mardi après-midi)
  * Globalement, les CM ont lieu le mardi après-midi de 14h à 15h30, les TD / TP le lundi matin de 8h à 9h30 et de 9h45 à 11h15 ou de 11h30 à 13h ; chaque créneau de 8h à 9h30 et de 9h45 à 11h15 ou de 11h3à à 13h est occupé soit par un TD / TP de théorie des langages, soit par un TD / TP de logique classique
  * À partir du 20 septembre
      * **TD** de LC **ou** de LF de 8h à 9h30
      * **TP** de LC **ou** de LF de 9h45 à 11h15 **ou** de 11h30 à 13h
  * Fiez-vous à votre emploi du temps sur ADE

### Intervenants

  * Sylvain Brandel (CM, TD et TP groupe A)
  * Emmanuel Coquery (TD et TP groupe B)
  * Fabien De Marchi (TD et TP groupe C)
  * Xavier Urbain (TD et TP groupe D)
  * Mohand-Said Hacid (TD groupe E)
  * Agathe Herrou (TP groupe E1)
  * Qi Qiu (TP groupe E2)

### Evaluation

  * **Contrôle continu intégral**  :
      * TP noté lundi 6 décembre 2021 8h ou 9h45
      * ECA mardi 7 décembre 2021 14h
      * Interros en début de TD, peut-être un autre TP noté …

----

## Annales

  * [Contrôle continu terminal 2008-2009](annales/lif11-ccfinal-2008-2009.pdf)
  * [Contrôle continu terminal 2009-2010](annales/lif11-ccfinal-2009-2010.pdf)
  * [Contrôle continu terminal 2009-2010](annales/lif11-ccfinal-2009-2010-corrige.pdf)
  * [Contrôle continu terminal 2010-2011](annales/lif11-ccfinal-2010-2011.pdf)
  * [Contrôle continu terminal 2011-2012](annales/lif15-ccfinal-2011-2012.pdf)
  * [Contrôle continu terminal 2012-2013](annales/lif15-ccfinal-2012-2013.pdf)
  * [Contrôle continu terminal 2013-2014](annales/lif15-ccf-v2-2013-2014.pdf)
  * [Contrôle continu terminal 2014-2015](annales/lif15-ccf-2014-2015.pdf)
  * [Contrôle continu terminal 2015-2016](annales/lif15-ccf-2015-2016.pdf)
  * [Examen session 1 2016-2017](annales/liflf-examen-s1-v2.pdf) [détail de la notation](annales/examen_notation.pdf)
  * [Examen QCM test](annales/liflf-examen-test.pdf)
  * [TP noté 2019-2020](annales/liflf_TPN_A_2019.v)



