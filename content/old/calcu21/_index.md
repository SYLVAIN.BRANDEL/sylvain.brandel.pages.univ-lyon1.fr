---
title: Mif09 - Calculabilité et complexité
linkTitle: old/calcu21
date: '2022-01-18'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2021 - 2022**</span>

---

## Modèles de calcul

Page pédagogique de l'UE [Mif09 - calculabilité et complexité](https://perso.liris.cnrs.fr/xavier.urbain/ens/CALCU/)

---

## Partie complexité

  * Mercredi 13 octobre 2021 - CM [classe P et NP](M1if09-CM07.pdf)
  * Vendredi 15 octobre 2021 - CM [NP-complet](M1if09-CM08.pdf)

## TD10 - vendredi 14 janvier 2022

  * Groupe B - 8h00 [vidéo](https://liris.cnrs.fr/sbrandel/medias/calcu_TD10_groupe_B_14_janvier_2022_8h00.mp4)
  * Groupe A - 9h45 [vidéo](https://liris.cnrs.fr/sbrandel/medias/calcu_TD10_groupe_A_14_janvier_2022_9h45.mp4)

