---
title: Mif21 - Insertion professionnelle
linkTitle: old/insertion22
date: '2023-02-15'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## Ressources

  * CM1 [[slides]](CM1.pdf) [[support]](CM1-support.pdf) L'entreprise, histoire, constitution, droit
  * CM2 [[slides]](CM2.pdf) [[support]](CM2-support.pdf) Droit du Travail, histoire et sources
  * CM3 [[slides]](CM3.pdf) [[support]](CM3-support.pdf) Instances représentatives du personnel
  * CM4 [[slides]](CM4.pdf) [[support]](CM4-support.pdf) Contrat de travail et fiche de paie
  * CM5 [[slides]](CM5.pdf) [[slides]](CM5bis.pdf) [[support]](CM5-support.pdf) Economie d'entreprise - Compétences
  * CM6 [[slides]](CM6.pdf) [[slides]](CM6bis.pdf) [[exemple]](CM6-exemple.pdf) Stratégie de maintenance - Communication et management

  * [[Vidéos]](https://www.youtube.com/channel/UCu2Kcdzf5o2WiHjfOFedHWg)



## Evaluation

<span style="color: red">Il n'y aura pas d'examen ou autre forme d'évaluation.</span>

L'UE sera validée sous forme d'une VACQ.

~~Un examen aura lieu vendredi 10 mars 2023 à 14h.
Cet examen sera sous forme de QCM et portera uniquement sur les 6 cours qui ont eu lieu en septembre - octobre 2022 : CM1 à CM5.~~

