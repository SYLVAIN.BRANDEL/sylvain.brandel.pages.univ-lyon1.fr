---
title: Mif10 - Projet transversal de master informatique
linkTitle: old/projet23
date: '2023-12-18'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2023 - 2024**</span>

---

Le projet transversal de master informatique se déroulera du 29 février au 3 mai 2024.

Page du cours : [[https://forge.univ-lyon1.fr/m1if10/2023]](https://forge.univ-lyon1.fr/m1if10/2023)

