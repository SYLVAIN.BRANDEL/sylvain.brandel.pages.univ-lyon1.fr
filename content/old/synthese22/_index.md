---
title: Mif27 - Synthèse d'images
linkTitle: old/synthese22
date: '2023-05-04'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## CM - Courbes et surfaces paramétriques et de subdivision

  * [[modelisation2.pdf]](https://liris.cnrs.fr/sbrandel/doc/M1/modelisation2.pdf)

----

## TP - Courbes paramétriques et de subdivision

L'objectif de cette séance est de construire des courbes paramétriques, et de subdiviser des courbes existantes.
La mise en place de la technique de visualisation de ces courbes devra prendre le moins de temps possible.

Pour visualiser vos courbes, vous pouvez générer un fichier au format Wavefront `.obj` par exemple, et le visualiser avec `Blender` par exemple.
Exemple de fichier `.obj` contenant une ligne polygonale :

```
# Sommets
v 0 1 1
v 0 1 0
v 1 1 0
v 1 0 0
v 0 0 0
v 0 0 1
v 1 0 1
v 1 1 1

# Ligne polygonale (L minuscule)
l 1 2 3 4 5 6 7 8 
```

Exemple de programme C à deux balles qui génère ce fichier :
```
#include <stdio.h>

struct Point3D {
  float x; float y; float z;
};

int main(void) {
  Point3D points[8] = { {0,1,1}, {0,1,0}, {1,1,0}, {1,0,0}, {0,0,0}, {0,0,1}, {1,0,1}, {1,1,1} };

  FILE* f = fopen("line.obj","w");

  for (int i=0;i<8;i++) {
    fprintf(f,"v %f %f %f\n",points[i].x,points[i].y,points[i].z);
  }

  fprintf(f,"\nl");
  for (int i=1;i<=8;i++) {
    fprintf(f," %d",i);
  }

  fclose(f);

  return 0;
}
```

Vous pouvez également utiliser votre base de code pour visualiser vos courbes.


### 1. Une courbe simple

Créez une ligne polygonale avec une dizaine de points.
Testez  votre solution de visualisation.


### 2. Courbes paramétriques

Votre ligne polygonale sert ici de polygone de contrôle.


#### a) Courbes de Bézier

Implémentez l'algorithme de De Casteljau (CM pages 34 et 35).
Calculez les points pour des valeurs du paramètre variant de 0 à 1 avec un pas de 0.01 par exemple.


#### b) (facultatif) Courbes B-Spline

Implémentez l'algorithme de De Boor - Cox (CM page 63).
Calculez les points pour des valeurs du paramètre variant de 0 à 1 avec un pas de 0.01 par exemple.
Testez avec un vecteur de noeuds répartis uniformément et confondus.


### 3. Courbes de subdivision

Vous allez subdiviser votre ligne polygonale.


#### a) Implémentation des différents schémas de subdivision de courbes

  * Schéma de Chaikin (CM page 101)
  * Schéma de Catmull-Clark (CM page 102)
  * Schéma 4 points (CM page 104)
  * Fractales (CM pages 108 et 109)


#### b) Génération de tubes

S'il reste du temps, vous pouvez compléter le schéma Chaikin en suivant le sujet suivant :

[[TP génération de tubes (V. Nivoliers)]](https://forge.univ-lyon1.fr/m1if27/courbes-etu/-/blob/master/readme.md)


