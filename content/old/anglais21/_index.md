---
title: Anglais pour la communication professionnelle niveau 1
linkTitle: old/anglais21
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2021 - 2022**</span>

---

## Planning et salles

### Groupe Rech

- Mme Mireille HUBER - [mireille.huber@univ-lyon1.fr](mailto:mireille.huber@univ-lyon1.fr)
- Les lundis après-midi jusqu'au 11 avril 2022 puis les mardis matins à partir du 3 mai 2022
- Salle Nautibus TD2 ou TD3 ou TD9 (voir ci-dessous)

|  |  |  |
| --- | --- | --- |
| Lundi 07/02/22 | 14h-17h15 | Nautibus TD2 |
| Lundi 14/02/22 | 14h-17h15 | Nautibus TD3 |
| Lundi 07/03/22 | 14h-17h15 | Nautibus TD2 |
| Lundi 14/03/22 | 14h-17h15 | Nautibus TD2 |
| <span style="color: red">Mardi 05/04/22</span> | 14h-17h15 | <span style="color: red">Nautibus TD6</span> |
| Lundi 11/04/22 | 14h-17h15 | Nautibus TD2 |
| <span style="color: red">Mardi</span> 03/05/22 | <span style="color: red">9h45-13h</span> | Nautibus TD9 |
| <span style="color: red">Mardi</span> 10/05/22 | <span style="color: red">9h45-13h</span> | Nautibus TD9 |
| <span style="color: red">Mardi</span> 31/05/22 | <span style="color: red">9h45-13h</span> | Nautibus TD9 |
| <span style="color: red">Mardi</span> 07/06/22 | <span style="color: red">9h45-13h</span> | Nautibus TD9 |


---

### Groupe Ent1

- Mme Patricia MIRAMAND-GRAVAND - [patricia.miramand-gravrand@univ-lyon1.fr](mailto:patricia.miramand-gravrand@univ-lyon1.fr)
- Les mardis matins 8h-11h15 salle Nautibus TD1

|  |  |  |
| --- | --- | --- |
| Mardi 08/02/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 15/02/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 08/03/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 15/03/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 05/04/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 12/04/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 03/05/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 10/05/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 31/05/22 | 8h-11h15 | Nautibus TD1 |
| Mardi 07/06/22 | 8h-11h15 | Nautibus TD1 |


---

### Groupe Ent2

- Mme Séverine FRACCOLA - [severine.fraccola@univ-lyon1.fr](mailto:severine.fraccola@univ-lyon1.fr)
- Les mardis matins 9h45-13h salles Nautibus TD3 et TD9 (voir ci-dessous)

|  |  |  |
| --- | --- | --- |
| Mardi 08/02/22 | 9h45-13h | Nautibus TD9 |
| Mardi 15/02/22 | 9h45-13h | Nautibus TD3 |
| Mardi 08/03/22 | 9h45-13h | Nautibus TD9 |
| Mardi 15/03/22 | 9h45-13h | Nautibus TD9 |
| Mardi 05/04/22 | 9h45-13h | Nautibus TD9 |
| Mardi 12/04/22 | 9h45-13h | Nautibus TD3 |
| Mardi 03/05/22 | 9h45-13h | Nautibus TD3 |
| Mardi 10/05/22 | 9h45-13h | Nautibus TD3 |
| Mardi 31/05/22 | 9h45-13h | Nautibus TD3 |
| Mardi 07/06/22 | 9h45-13h | Nautibus TD3 |


---

### Groupe Ent3

- Mme Myriam LACOTE - [myriam.lacote@univ-lyon1.fr](mailto:myriam.lacote@univ-lyon1.fr)
- Les mardis après-midi 14h-17h15 salle Nautibus TD2 ou TD3 ou TD9 (voir ci-dessous)

|  |  |  |
| --- | --- | --- |
| Mardi 08/02/22 | 14h-17h15 | Nautibus TD3 |
| <span style="color: red">~~Mardi 15/02/22~~</span> | <span style="color: red">~~14h-17h15~~</span> | <span style="color: red">~~Nautibus TD3~~</span> |
| <span style="color: red">~~Mardi 08/03/22~~</span> | <span style="color: red">~~14h-17h15~~</span> | <span style="color: red">~~Nautibus TD2~~</span> |
| Mardi 15/03/22 | 14h-17h15 | Nautibus TD2 |
| <span style="color: red">~~Mardi 05/04/22~~</span> | <span style="color: red">~~14h-17h15~~</span> | <span style="color: red">~~Nautibus TD2~~</span> |
| <span style="color: red">~~Mardi 12/04/22~~</span> | <span style="color: red">~~14h-17h15~~</span> | <span style="color: red">~~Nautibus TD9~~</span> |
| Mardi 03/05/22 | 14h-17h15 | Nautibus TD2 |
| Mardi 10/05/22 | 14h-17h15 | Nautibus TD2 |
| Mardi 31/05/22 | 14h-17h15 | Nautibus TD2 |
| Mardi 07/06/22 | 14h-17h15 | Nautibus TD2 |


---

### Groupe Ent4

- M. Olivier BOUGUIN-VASILJEVIC - [olivier.bouguin-vasiljevic@univ-lyon1.fr](mailto:olivier.bouguin-vasiljevic@univ-lyon1.fr)
- Les mercredis après-midi 14h-17h15 salle Nautibus TD2 ou TD3 (voir ci-dessous)

|  |  |  |
| --- | --- | --- |
| Mercredi 09/02/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 16/02/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 09/03/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 16/03/22 | 14h-17h15 | Nautibus TD3 |
| Mercredi 06/04/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 13/04/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 04/05/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 11/05/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 01/06/22 | 14h-17h15 | Nautibus TD2 |
| Mercredi 08/06/22 | 14h-17h15 | Nautibus TD2 |


---

## Organisation

1 groupe d'anglais pour la recherche, 4 groupes d'anglais pour l'entreprise.

Les enseignements d'anglais ont lieu pendant les semaines oranges - alternants présents.


