---
title: LifLF - Langages formels
linkTitle: old/langages22
date: '2021-11-05'
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2022 - 2023**</span>

---

## Progression et ressources pédagogiques
|    |                                      |      |   |
|----|------------- | -----------------------|--------------------------------------|---|
| 1 |lundi 05/09/22 | 9h45-11h15             |<span style="color: orange">CM0</span>|[[Introduction]](LF-CM00.pdf)
|   |               |                        |<span style="color: orange">CM1</span>|[[Rappels maths]](LF-CM01.pdf)
|   |mardi 06/09/22 | 14h-15h30              |<span style="color: orange">CM2</span>|[[Alphabets et langages]](LF-CM02.pdf)
| 2 |lundi 12/09/22 | 9h45-11h15             |<span style="color: green"> TD1</span>|Ensembles et relations (sujet ci-dessous)
|   |mardi 13/09/22 | 14h-15h30              |<span style="color: orange">CM3</span>|[[Grammaires]](LF-CM03.pdf)
| 3 |lundi 19/09/22 | 9h45-11h15             |<span style="color: green"> TD2</span>|Alphabets, langages, représentation finie
|   |mardi 20/09/22 | 14h-15h30              |<span style="color: orange">CM4</span>|[[Automates à états finis]](LF-CM04.pdf)
| 4 |lundi 26/09/22 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP1</span>|Prog. fonctionnelle en Coq / Gallina [[sujet]](LF-TP1.pdf) [[TP1.v]](tp/LF-TP1.v) [[TP1-corr-etu.v]](tp/LF-TP1-corr-etu.v)
|   |mardi 27/09/22 | 14h-15h30              |<span style="color: orange">CM5</span>|[[Déterminisation]](LF-CM05.pdf)
| 5 |lundi 03/10/22 | 8h-9h30                |<span style="color: green"> TD3</span>|Automates à états finis - déterminisation
|   |mardi 04/10/22 | 14h-15h30              |<span style="color: orange">CM6</span>|[[Caractérisation]](LF-CM06.pdf)
| 6 |lundi 10/10/22 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP2</span>|Automates en Coq (part. 1) [[sujet]](LF-TP2.pdf) [[TP2.v]](tp/LF-TP2.v) [[TP2-corr-etu.v]](tp/LF-TP2-corr-etu.v)
|   |mardi 11/10/22 | 14h-15h30              |<span style="color: orange">CM7</span>|[[Minimisation des états]](LF-CM07.pdf)
| 7 |lundi 17/10/22 | 8h-9h30                |<span style="color: green"> TD4</span>|Caractérisation - automate standard
|   |mardi 18/10/22 | 14h-15h30              |<span style="color: orange">CM8</span>|[[Langages rationnels - rationalité]](LF-CM08.pdf)
| 8 |lundi 24/10/22 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP3</span>|Automates en Coq (part. 2) [[sujet]](LF-TP3.pdf) [[TP3.v]](tp/LF-TP3.v) [[TP3-corr-etu.v]](tp/LF-TP3-corr-etu.v)
|   |mardi 25/10/22 | 14h-15h30              |<span style="color: orange">CM9</span>|[[Automates à pile - algébricité]](LF-CM09.pdf)
|   |               |                        |                                      | Vacances universitaires
| 9 |lundi 07/11/22 | 8h-9h30                |<span style="color: green"> TD5</span>|Lemme d'Arden - rationalité
|   |mardi 08/11/22 | 14h-15h30              |<span style="color: orange">CM10</span>|[[Analyse ascendante]](LF-CM10.pdf)
| 10|lundi 14/11/22 | 9h45-11h15             |<span style="color: green"> TD6</span>|Grammaires - analyse ascendante
| 11|lundi 21/11/22 | 9h45-11h15 ou 11h30-13h|<span style="color: purple">TP4</span>|Grammaires et automates en Coq [[sujet]](LF-TP4.pdf) [[TP4.v]](tp/LF-TP4.v) 
| 12|lundi 28/11/22 |                        |                                      | (pas d'enseignements de langages formels cette semaine)
| 13|lundi 05/12/22 | 8h-13h                 |<span style="color: green"> TPN</span>|**TP noté**
|   |mardi 06/12/22 | 14h-15h30              |                            ECA        |**Epreuve commune anonyme**


### Ressources complémentaires

  * [[Exercices de TD]](LF-TD.pdf)


----



## Instructions pour les TP

  * Téléchargez le sujet (lien ci-dessus dans la progression), ouvrez-le dans CoqIDE, lisez-le et complétez-le.

On parle bien de **CoqIDE**, c'est-à-dire l'interface graphique permettant d'invoquer Coq. Installer Coq tout court ne vous servira pas à grand chose. Les installeurs de CoqIDE incluent Coq.

  * Si CoqIDE non disponible : [JsCoq](https://jscoq.github.io/scratchpad.html) Collez le sujet du TP dans le cadre de gauche

Pour voir correctement le sujet dans votre **navigateur**, configurez l'encodage par défaut de votre navigateur en Unicode (UTF-8)

Pour installer CoqIDE sur votre machine :

  * MacOS :
      * Installeur binaire sur [https://github.com/coq/platform/releases/latest](https://github.com/coq/platform/releases/latest) (inclut CoqIDE)
      * Avec [Homebrew](https://brew.sh/index_fr) : `brew install coqide`

  * Windows : plusieurs manières d'installer CoqIDE, cf. [Installer Coq sous Windows](https://github.com/coq/coq/wiki/Installation%20of%20Coq%20on%20Windows) :
      * Installeur binaire sur [https://github.com/coq/platform/releases/latest](https://github.com/coq/platform/releases/latest) (inclut CoqIDE)
      * Installer Linux dans une VM ou dans WSL puis suivre les instructions pour Linux
  * Linux : cf. [Installer Coq sous Linux](https://github.com/coq/coq/wiki/Installation%20of%20Coq%20on%20Linux)

----

## Organisation

### Responsables

  * LifLF - Théorie des langages formels : Sylvain Brandel
  * LifLC - Logique classique : Xavier Urbain, [page pédagogique de logique classique](https://perso.liris.cnrs.fr/xavier.urbain/enseignement/)

### Volume

  * CM : 15 heures (10 x 1h30)
  * TD : 9 heures (6 x 1h30)
  * TP : 6 heures (4 x 1h30)

### Horaires

  * Tous les enseignements de LifLF (et LifLC) ont lieu dans la séquence 1 (lundi matin et mardi après-midi)
  * Globalement, les CM ont lieu le mardi après-midi de 14h à 15h30, les TD / TP le lundi matin de 8h à 9h30 et de 9h45 à 11h15 ou de 11h30 à 13h ; chaque créneau de 8h à 9h30 et de 9h45 à 11h15 ou de 11h3à à 13h est occupé soit par un TD / TP de théorie des langages, soit par un TD / TP de logique classique
  * À partir du 26 septembre 2022
      * **TD** de LC **ou** de LF de 8h à 9h30
      * **TP** de LC **ou** de LF de 9h45 à 11h15 **ou** de 11h30 à 13h
  * Fiez-vous à votre emploi du temps sur ADE

### Intervenants

  * Sylvain Brandel (CM, TD et TP groupe A)
  * Emmanuel Coquery (TD et TP groupe B)
  * Fabien De Marchi (TD et TP groupe C)
  * Xavier Urbain (TD et TP groupe D)
  * Qi Qiu (TD et TP groupe E, début du semestre)
  * Mohand-Said Hacid (TD et TP groupe E, fin du semestre)

### Evaluation

  * **Contrôle continu intégral**  :
      * TP noté lundi 5 décembre 2022 8h ou 9h45
      * ECA mardi 6 décembre 2022 14h
      * Interros en début de TD, peut-être un autre TP noté …
      * Rattrapage en juin 3033

----

## Annales

  * [Contrôle continu terminal 2008-2009](annales/lif11-ccfinal-2008-2009.pdf)
  * [Contrôle continu terminal 2009-2010](annales/lif11-ccfinal-2009-2010.pdf)
  * [Contrôle continu terminal 2009-2010](annales/lif11-ccfinal-2009-2010-corrige.pdf)
  * [Contrôle continu terminal 2010-2011](annales/lif11-ccfinal-2010-2011.pdf)
  * [Contrôle continu terminal 2011-2012](annales/lif15-ccfinal-2011-2012.pdf)
  * [Contrôle continu terminal 2012-2013](annales/lif15-ccfinal-2012-2013.pdf)
  * [Contrôle continu terminal 2013-2014](annales/lif15-ccf-v2-2013-2014.pdf)
  * [Contrôle continu terminal 2014-2015](annales/lif15-ccf-2014-2015.pdf)
  * [Contrôle continu terminal 2015-2016](annales/lif15-ccf-2015-2016.pdf)
  * [Examen session 1 2016-2017](annales/liflf-examen-s1-v2.pdf) [détail de la notation](annales/examen_notation.pdf)
  * [Examen QCM test](annales/liflf-examen-test.pdf)
  * [TP noté 2019-2020](annales/liflf_TPN_A_2019.v)



