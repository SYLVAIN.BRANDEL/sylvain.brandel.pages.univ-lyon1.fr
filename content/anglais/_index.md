---
title: Anglais pour la communication professionnelle niveau 1
linkTitle: anglais
type: book

view: 2

header:
  caption: ""
  image: ""
---

<span style="color: red">**ANNEE 2024 - 2025**</span>

---

## Organisation

- 1 groupe d'anglais pour la recherche, 6 groupes d'anglais pour l'entreprise.
- Les enseignements d'anglais ont lieu pendant les semaines oranges - alternants présents.
- 8 séances de 3h par groupe.

![Planning global](planning_anglais_24-25.png)

Intervenants :
- Rech : Mme Mireille HUBER - [mireille.huber@univ-lyon1.fr](mailto:mireille.huber@univ-lyon1.fr)
- Ent1 : M. Jacques PALESTRA - [jacques.palestra@univ-lyon1.fr](mailto:jacques.palestra@univ-lyon1.fr)
- Ent2 : Mme Myriam LACOTE - [myriam.lacote@univ-lyon1.fr](mailto:myriam.lacote@univ-lyon1.fr)
- Ent3 : Olivier BOUGUIN-VASILJEVIC - [olivier.bouguin-vasiljevic@univ-lyon1.fr](mailto:olivier.bouguin-vasiljevic@univ-lyon1.fr)
- Ent4 : Mme Patricia MIRAMAND-GRAVAND - [patricia.miramand-gravrand@univ-lyon1.fr](mailto:patricia.miramand-gravrand@univ-lyon1.fr)
- Ent5 : Mme Carissa SIMS - [carissa.sims@univ-lyon1.fr](mailto:carissa.sims@univ-lyon1.fr)
- Ent6 : Mme Marie GUEGUEN - [marie.gueguen@univ-lyon1.fr](mailto:marie.gueguen@univ-lyon1.fr)



---


